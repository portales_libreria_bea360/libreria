"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNotes = getNotes;
exports.addNote = addNote;

var _require = require('../../util/axios'),
    axiosBea360 = _require.axiosBea360;

function getNotes(data) {
  var url = "casos/notas/getby";
  var instance = axiosBea360(url);
  return instance.get('/', {
    params: {
      idobjeto: data.idobjeto,
      tipoobjeto: data.tipoobjeto
    }
  });
}

function addNote(data) {
  var url = "casos/notas/add";
  var instance = axiosBea360(url);
  return instance.post('', {
    idobjeto: data.idobjeto,
    tipoobjeto: data.tipoobjeto,
    texto: data.texto
  });
}