"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.paises = void 0;
var paises = {
  baseURL: 'https://rapidapi.p.rapidapi.com',
  actions: [{
    action: 'get_paises',
    path: '/location/country/list',
    method: 'get',
    key1: 'countries-cities.p.rapidapi.com',
    key2: '13bc5b1393mshc7786c040319b92p159d7fjsn8bf598f8c88e'
  }, {
    action: 'get_info_covid',
    path: '/v1/total',
    method: 'get',
    key1: 'covid-19-coronavirus-statistics.p.rapidapi.com',
    key2: '13bc5b1393mshc7786c040319b92p159d7fjsn8bf598f8c88e'
  }]
};
exports.paises = paises;