"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getArchivos = getArchivos;
exports.upload = upload;
exports.downLoad = downLoad;
exports.destroyArchivo = destroyArchivo;

var _require = require('../../util/axios'),
    axiosBea360 = _require.axiosBea360;

var axios = require('axios');

var _require2 = require('file-saver'),
    saveAs = _require2.saveAs;

var get = require('lodash/get');

var _require3 = require('../../util/instance'),
    Instances = _require3.Instances;

var _require4 = require('../../util/methods'),
    encode = _require4.encode;

var con = new Instances();

function getArchivos(data) {
  var url = "casos/archivo/getby";
  var instance = axiosBea360(url);
  return instance.get('/', {
    params: {
      idobjeto: data.idobjeto,
      tipoobjeto: data.tipoobjeto
    }
  });
}

function upload(data) {
  if (!data.dataToSend.file) {
    return Promise.resolve();
  }

  var dataToSend = {
    companyname: con.getContacto().company,
    file: data.dataToSend.file,
    filesize: data.dataToSend.filesize,
    idobjeto: data.dataToSend.idobjeto,
    tags: data.dataToSend.tags,
    tipoobjeto: data.dataToSend.tipoobjeto
  };
  var formData = new FormData();

  for (var i in dataToSend) {
    formData.append(i.toUpperCase(), dataToSend[i]);
  }

  return axios.post(data.endpoint, formData, {
    baseURL: con.getContacto().url,
    headers: {
      'Content-Type': 'multipart/form-data',
      "Authorization": "Basic ".concat(encode(con.getContacto(), "token"))
    }
  }).then(function (response) {
    var data = get(response, 'data', {});
    return data;
  });
}

function downLoad(data) {
  var formData = new FormData();
  var dataToSend = {
    companyname: con.getContacto().company,
    id: data.dataToSend.id
  };

  for (var i in dataToSend) {
    formData.append(i.toUpperCase(), dataToSend[i]);
  }

  return axios.post(data.endpoint, formData, {
    baseURL: con.getContacto().url,
    responseType: 'blob',
    headers: {
      'Content-Type': 'multipart/form-data',
      "Authorization": "Basic ".concat(encode(con.getContacto(), "token"))
    }
  }).then(function (response) {
    var dat = get(response, 'data', {});
    saveAs(new Blob([dat]), data.name);
    return dat;
  });
}

function destroyArchivo(data) {
  var url = "casos/archivo/del";
  var instance = axiosBea360(url);
  return instance["delete"]('/', {
    params: {
      id: data.id
    }
  });
}