"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getParametros = getParametros;

var _require = require('../../util/axios'),
    axiosBea360 = _require.axiosBea360;

function getParametros(data) {
  var url = "prparametros/get";
  var instance = axiosBea360(url);
  return instance.get('', {
    params: {
      parametro: data.parametro
    }
  });
}