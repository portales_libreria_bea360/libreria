"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllCuenta = getAllCuenta;

var _require = require('../../util/axios'),
    axiosBea360 = _require.axiosBea360;

function getAllCuenta(data) {
  var url = "cuenta/get";
  var instance = axiosBea360(url);
  return instance.get();
}