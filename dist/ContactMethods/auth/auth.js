"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginCp = loginCp;
exports.logoutCp = logoutCp;
exports.getByContact = getByContact;
exports.addContact = addContact;
exports.updContact = updContact;
exports.updatePass = updatePass;
exports.recoveryPass = recoveryPass;

/**funcion que permite hacer el login del usuario del portal
 * por parametro llega un objetoUser con tres variables 
 */
var axios = require('axios');

var _require = require('../../util/axios'),
    axiosBea360 = _require.axiosBea360,
    axiosBea360Register = _require.axiosBea360Register;

var _require2 = require('../../util/methods'),
    cleanData = _require2.cleanData,
    encode = _require2.encode,
    cleanDataRecovery = _require2.cleanDataRecovery;

var _require3 = require('../../util/instance'),
    Instances = _require3.Instances;

function loginCp(data) {
  var user = {
    "company": data.company,
    "email": data.email,
    "pass": data.pass
  };

  try {
    return axios.post("".concat(data.url, "/apir/v10/cp/auth/login"), user).then(function (response) {
      if (response.data.contacto != null) {
        var instance = new Instances();
        instance.createContacto({
          contacto: response.data.contacto,
          company: data.company,
          url: data.url
        });
        return {
          data: response.data
        };
      }
    });
  } catch (e) {
    console.log(e);
    return null;
  }
}

function logoutCp(data) {
  var instance = new Instances();
  instance.deleteContacto(data);
}

function getByContact(data) {
  var url = "perfil/getby";
  var instance = axiosBea360(url);
  var params = cleanData(data);
  return instance.get('', {
    params: params
  });
}

function addContact(data) {
  var url = "auth/registro";
  var instance = axiosBea360Register(data, url);
  var params = cleanDataRecovery(data);
  return instance.post('', params);
}

function updContact(data) {
  var url = "perfil/upd";
  var instance = axiosBea360(url);
  var params = cleanData(data);
  return instance.put('', params);
}

function updatePass(data) {
  var url = "perfil/cambiopass";
  var instance = axiosBea360(url);
  var params = cleanData(data);
  return instance.put('', params);
}

function recoveryPass(data) {
  //cambiar el metodo axios
  var url = "auth/crecovery";
  var instance = axiosBea360Register(data, url);
  var params = cleanDataRecovery(data);
  return instance.post('', params);
}