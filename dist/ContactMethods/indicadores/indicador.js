"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getIndicador = getIndicador;

/**
 * metodos mara mostrar indicadores de los casos totales, por mes, finlizados etc.
 */
function getIndicador(value) {
  var casos = value;
  var casosTotal = {
    "total": casos.length
  };
  var finalizado = {
    "total": 0,
    "porcentaje": 0
  };
  var proceso = {
    "total": 0,
    "porcentaje": 0
  };
  var casosMes = {
    "total": 0
  };
  var indicadores;
  var hoy = new Date();
  var mes = hoy.getMonth() + 1;
  var año = hoy.getFullYear();
  casos.forEach(function (caso) {
    if (caso.finalizado === 1) {
      finalizado.total++;
    } else {
      proceso.total++;
    }

    if (parseInt(caso.fechacreacion.split('-')[1]) === mes && parseInt(caso.fechacreacion.split('-')[0]) === año) {
      casosMes.total++;
    }
  });
  /**porcentaje */

  finalizado.porcentaje = calculoPorcentaje(casosTotal.total, finalizado.total);
  proceso.porcentaje = calculoPorcentaje(casosTotal.total, proceso.total);
  return indicadores = {
    casosTotal: casosTotal,
    finalizado: finalizado,
    proceso: proceso,
    casosMes: casosMes
  };
}

function calculoPorcentaje(total, finalizado) {
  return Math.floor(finalizado / total * 100 * 100) / 100;
}