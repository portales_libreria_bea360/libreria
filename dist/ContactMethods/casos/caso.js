"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAll = getAll;
exports.getBy = getBy;
exports.addCase = addCase;
exports.getProductos = getProductos;
exports.getMotivos = getMotivos;
exports.getSubMotivos = getSubMotivos;

/** funcion que trae todos los casos asociados a un contacto
 * por parametro llega el objeto casos que dentro contiene dos variables 
 * casos.url(base de datos del cliente) y casos.key(variable encriptada)
 * dentro la funcion invoca un metodo que hace la peticion GET al servidor bea360
 */
var _require = require('../../util/axios'),
    axiosBea360 = _require.axiosBea360;

var _require2 = require('../../util/methods'),
    cleanData = _require2.cleanData;

function getAll(data) {
  var url = "casos/get";
  var instance = axiosBea360(url);
  return instance.get();
}

function getBy(data) {
  var url = "casos/getby";
  var instance = axiosBea360(url);
  return instance.get('/', {
    params: {
      id: data.id
    }
  });
}

function addCase(data) {
  var url = "casos/add";
  var instance = axiosBea360(url);
  var params = cleanData(data);
  return instance.post('/', params);
}

function getProductos(data) {
  var url = "casos/producto/get";
  var instance = axiosBea360(url);
  return instance.get();
}

function getMotivos(data) {
  var url = "casos/tipo/getby";
  var instance = axiosBea360(url);
  return instance.get('/', {
    params: {
      idproducto: data.idproducto
    }
  });
}

function getSubMotivos(data) {
  var url = "casos/subtipo/getby";
  var instance = axiosBea360(url);
  return instance.get('/', {
    params: {
      idproducto: data.idproducto,
      idtipo: data.idtipo
    }
  });
}