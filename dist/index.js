"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _portalBea = require("./lib/portalBea360");

Object.keys(_portalBea).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _portalBea[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _portalBea[key];
    }
  });
});

var _agenteBea = require("./lib/agenteBea360");

Object.keys(_agenteBea).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _agenteBea[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _agenteBea[key];
    }
  });
});