"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Util = exports.Agente_parametro = exports.Agente_check = exports.Agente_archivo = exports.Agente_nota = exports.Agente_grupo = exports.Agente_sla = exports.Agente_cf = exports.Agente_proceso = exports.Agente_cuenta = exports.Agente_estado = exports.Agente_prioridad = exports.Agente_user = exports.Agente_contacto = exports.Agente_tipificacion = exports.Agente_caso = exports.Agente_auth = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var _require = require('../UserMethods/auth/auth'),
    userToken = _require.userToken,
    userLogin = _require.userLogin,
    authwithtoken = _require.authwithtoken;

var _require2 = require('../UserMethods/caso/caso'),
    getAll = _require2.getAll,
    getBy = _require2.getBy,
    updCase = _require2.updCase,
    addCase = _require2.addCase,
    asignadoGrupo = _require2.asignadoGrupo;

var _require3 = require('../UserMethods/nota/nota'),
    getAllNotas = _require3.getAllNotas,
    addNotas = _require3.addNotas;

var _require4 = require('../UserMethods/archivo/archivo'),
    getAllArchivos = _require4.getAllArchivos,
    addArchivos = _require4.addArchivos,
    downLoad = _require4.downLoad,
    destroyArchivo = _require4.destroyArchivo;

var _require5 = require('../UserMethods/tipificacion/tipificacion'),
    getAllProductos = _require5.getAllProductos,
    getAllMotivos = _require5.getAllMotivos,
    getAllSubMotivos = _require5.getAllSubMotivos,
    getOneProductos = _require5.getOneProductos,
    getOneMotivos = _require5.getOneMotivos,
    getOneSubMotivos = _require5.getOneSubMotivos;

var _require6 = require('../UserMethods/prioridad/prioridad'),
    getAllPrioridad = _require6.getAllPrioridad;

var _require7 = require('../UserMethods/estado/estado'),
    getAllEstados = _require7.getAllEstados;

var _require8 = require('../UserMethods/contacto/contacto'),
    getAllContacto = _require8.getAllContacto,
    getByContacto = _require8.getByContacto,
    addContacto = _require8.addContacto,
    updContacto = _require8.updContacto;

var _require9 = require('../UserMethods/usuario/usuario'),
    getAllUsuario = _require9.getAllUsuario,
    getByUsuario = _require9.getByUsuario,
    getUsuarioDisponible = _require9.getUsuarioDisponible;

var _require10 = require('../UserMethods/acuerdoSla/acuerdoSla'),
    getAllAcuerdo = _require10.getAllAcuerdo;

var _require11 = require('../UserMethods/grupoSkill/grupoSkill'),
    getByGruposSkill = _require11.getByGruposSkill;

var _require12 = require('../UserMethods/cuenta/cuenta'),
    getAllCuentas = _require12.getAllCuentas,
    getByCuenta = _require12.getByCuenta,
    addCuenta = _require12.addCuenta,
    updCuenta = _require12.updCuenta,
    getContacts = _require12.getContacts;

var _require13 = require('../UserMethods/proceso/proceso'),
    getAllProcesos = _require13.getAllProcesos,
    nextCase = _require13.nextCase,
    backCase = _require13.backCase,
    stagesCase = _require13.stagesCase;

var _require14 = require('../UserMethods/customfield/customfield'),
    getAllCfs = _require14.getAllCfs;

var _require15 = require('../util/methods'),
    _encode = _require15.encode,
    _decode = _require15.decode,
    cleanResponse = _require15.cleanResponse,
    getAPIs = _require15.getAPIs;

var _require16 = require('../UserMethods/checklist/index'),
    getAllChecklist = _require16.getAllChecklist,
    executescriptCheck = _require16.executescriptCheck,
    doneCheck = _require16.doneCheck,
    undoneCheck = _require16.undoneCheck;

var _require17 = require('../UserMethods/parametro/index'),
    getParametros = _require17.getParametros;

var _require18 = require('../util/instance'),
    Instances = _require18.Instances;

var Agente_auth = /*#__PURE__*/function () {
  function Agente_auth() {
    _classCallCheck(this, Agente_auth);
  }

  _createClass(Agente_auth, [{
    key: "loginToken",
    value: function () {
      var _loginToken = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(data) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.t0 = cleanResponse;
                _context.next = 3;
                return userToken(data);

              case 3:
                _context.t1 = _context.sent;
                return _context.abrupt("return", (0, _context.t0)(_context.t1));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function loginToken(_x) {
        return _loginToken.apply(this, arguments);
      }

      return loginToken;
    }()
  }, {
    key: "login",
    value: function () {
      var _login = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(data) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.t0 = cleanResponse;
                _context2.next = 3;
                return userLogin(data);

              case 3:
                _context2.t1 = _context2.sent;
                return _context2.abrupt("return", (0, _context2.t0)(_context2.t1));

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function login(_x2) {
        return _login.apply(this, arguments);
      }

      return login;
    }()
  }, {
    key: "loginSE",
    value: function () {
      var _loginSE = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(data) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", authwithtoken(data));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function loginSE(_x3) {
        return _loginSE.apply(this, arguments);
      }

      return loginSE;
    }()
  }, {
    key: "one",
    value: function one(data) {
      var instance = new Instances();

      switch (data) {
        case 'usuario':
          return instance.getUser().usuario;

        case 'company':
          return instance.getUser().company;

        case 'permiso':
          return instance.getUser().permisos;

        default:
          return instance.getUser('all');
      }
    }
  }]);

  return Agente_auth;
}();

exports.Agente_auth = Agente_auth;

var Agente_caso = /*#__PURE__*/function () {
  function Agente_caso() {
    _classCallCheck(this, Agente_caso);
  }

  _createClass(Agente_caso, [{
    key: "all",
    value: function () {
      var _all = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(data) {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.t0 = cleanResponse;
                _context4.next = 3;
                return getAll(data);

              case 3:
                _context4.t1 = _context4.sent;
                return _context4.abrupt("return", (0, _context4.t0)(_context4.t1));

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function all(_x4) {
        return _all.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "one",
    value: function () {
      var _one = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(data) {
        var method, id, caso, APIS;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                method = data.method, id = data.id;
                _context5.t0 = cleanResponse;
                _context5.next = 4;
                return getBy({
                  id: id
                });

              case 4:
                _context5.t1 = _context5.sent;
                caso = (0, _context5.t0)(_context5.t1);
                _context5.next = 8;
                return getAPIs(data, caso, 'caso');

              case 8:
                APIS = _context5.sent;

                if (!method) {
                  _context5.next = 19;
                  break;
                }

                _context5.prev = 10;
                return _context5.abrupt("return", _objectSpread({
                  data: caso
                }, APIS));

              case 14:
                _context5.prev = 14;
                _context5.t2 = _context5["catch"](10);
                console.log("error", _context5.t2);

              case 17:
                _context5.next = 20;
                break;

              case 19:
                return _context5.abrupt("return", caso);

              case 20:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[10, 14]]);
      }));

      function one(_x5) {
        return _one.apply(this, arguments);
      }

      return one;
    }()
  }, {
    key: "add",
    value: function () {
      var _add = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(data) {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.t0 = cleanResponse;
                _context6.next = 3;
                return addCase(data);

              case 3:
                _context6.t1 = _context6.sent;
                return _context6.abrupt("return", (0, _context6.t0)(_context6.t1));

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function add(_x6) {
        return _add.apply(this, arguments);
      }

      return add;
    }()
  }, {
    key: "upd",
    value: function () {
      var _upd = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(data) {
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.t0 = cleanResponse;
                _context7.next = 3;
                return updCase(data);

              case 3:
                _context7.t1 = _context7.sent;
                return _context7.abrupt("return", (0, _context7.t0)(_context7.t1));

              case 5:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));

      function upd(_x7) {
        return _upd.apply(this, arguments);
      }

      return upd;
    }()
  }, {
    key: "asignadoagrupo",
    value: function () {
      var _asignadoagrupo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(data) {
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.t0 = cleanResponse;
                _context8.next = 3;
                return asignadoGrupo(data);

              case 3:
                _context8.t1 = _context8.sent;
                return _context8.abrupt("return", (0, _context8.t0)(_context8.t1));

              case 5:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));

      function asignadoagrupo(_x8) {
        return _asignadoagrupo.apply(this, arguments);
      }

      return asignadoagrupo;
    }()
  }]);

  return Agente_caso;
}();

exports.Agente_caso = Agente_caso;

var Agente_nota = /*#__PURE__*/function () {
  function Agente_nota() {
    _classCallCheck(this, Agente_nota);
  }

  _createClass(Agente_nota, [{
    key: "all",
    value: function () {
      var _all2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(data) {
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.t0 = cleanResponse;
                _context9.next = 3;
                return getAllNotas(data);

              case 3:
                _context9.t1 = _context9.sent;
                return _context9.abrupt("return", (0, _context9.t0)(_context9.t1));

              case 5:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }));

      function all(_x9) {
        return _all2.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "add",
    value: function () {
      var _add2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(data) {
        return regeneratorRuntime.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                _context10.t0 = cleanResponse;
                _context10.next = 3;
                return addNotas(data);

              case 3:
                _context10.t1 = _context10.sent;
                return _context10.abrupt("return", (0, _context10.t0)(_context10.t1));

              case 5:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }));

      function add(_x10) {
        return _add2.apply(this, arguments);
      }

      return add;
    }()
  }]);

  return Agente_nota;
}();

exports.Agente_nota = Agente_nota;

var Agente_archivo = /*#__PURE__*/function () {
  function Agente_archivo() {
    _classCallCheck(this, Agente_archivo);
  }

  _createClass(Agente_archivo, [{
    key: "all",
    value: function () {
      var _all3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(data) {
        return regeneratorRuntime.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.t0 = cleanResponse;
                _context11.next = 3;
                return getAllArchivos(data);

              case 3:
                _context11.t1 = _context11.sent;
                return _context11.abrupt("return", (0, _context11.t0)(_context11.t1));

              case 5:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }));

      function all(_x11) {
        return _all3.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "add",
    value: function () {
      var _add3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(data) {
        return regeneratorRuntime.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.t0 = cleanResponse;
                _context12.next = 3;
                return addArchivos(data);

              case 3:
                _context12.t1 = _context12.sent;
                return _context12.abrupt("return", (0, _context12.t0)(_context12.t1));

              case 5:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }));

      function add(_x12) {
        return _add3.apply(this, arguments);
      }

      return add;
    }()
  }, {
    key: "download",
    value: function () {
      var _download = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13(data) {
        return regeneratorRuntime.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _context13.t0 = cleanResponse;
                _context13.next = 3;
                return downLoad(data);

              case 3:
                _context13.t1 = _context13.sent;
                return _context13.abrupt("return", (0, _context13.t0)(_context13.t1));

              case 5:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13);
      }));

      function download(_x13) {
        return _download.apply(this, arguments);
      }

      return download;
    }()
  }, {
    key: "destroy",
    value: function () {
      var _destroy = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14(data) {
        return regeneratorRuntime.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _context14.t0 = cleanResponse;
                _context14.next = 3;
                return destroyArchivo(data);

              case 3:
                _context14.t1 = _context14.sent;
                return _context14.abrupt("return", (0, _context14.t0)(_context14.t1));

              case 5:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14);
      }));

      function destroy(_x14) {
        return _destroy.apply(this, arguments);
      }

      return destroy;
    }()
  }]);

  return Agente_archivo;
}();

exports.Agente_archivo = Agente_archivo;

var Agente_tipificacion = /*#__PURE__*/function () {
  function Agente_tipificacion() {
    _classCallCheck(this, Agente_tipificacion);
  }

  _createClass(Agente_tipificacion, [{
    key: "productosAll",
    value: function () {
      var _productosAll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee15(data) {
        return regeneratorRuntime.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                _context15.t0 = cleanResponse;
                _context15.next = 3;
                return getAllProductos(data);

              case 3:
                _context15.t1 = _context15.sent;
                return _context15.abrupt("return", (0, _context15.t0)(_context15.t1));

              case 5:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15);
      }));

      function productosAll(_x15) {
        return _productosAll.apply(this, arguments);
      }

      return productosAll;
    }()
  }, {
    key: "productosOne",
    value: function () {
      var _productosOne = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee16(data) {
        return regeneratorRuntime.wrap(function _callee16$(_context16) {
          while (1) {
            switch (_context16.prev = _context16.next) {
              case 0:
                _context16.t0 = cleanResponse;
                _context16.next = 3;
                return getOneProductos(data);

              case 3:
                _context16.t1 = _context16.sent;
                return _context16.abrupt("return", (0, _context16.t0)(_context16.t1));

              case 5:
              case "end":
                return _context16.stop();
            }
          }
        }, _callee16);
      }));

      function productosOne(_x16) {
        return _productosOne.apply(this, arguments);
      }

      return productosOne;
    }()
  }, {
    key: "motivosAll",
    value: function () {
      var _motivosAll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee17(data) {
        return regeneratorRuntime.wrap(function _callee17$(_context17) {
          while (1) {
            switch (_context17.prev = _context17.next) {
              case 0:
                _context17.t0 = cleanResponse;
                _context17.next = 3;
                return getAllMotivos(data);

              case 3:
                _context17.t1 = _context17.sent;
                return _context17.abrupt("return", (0, _context17.t0)(_context17.t1));

              case 5:
              case "end":
                return _context17.stop();
            }
          }
        }, _callee17);
      }));

      function motivosAll(_x17) {
        return _motivosAll.apply(this, arguments);
      }

      return motivosAll;
    }()
  }, {
    key: "motivosOne",
    value: function () {
      var _motivosOne = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee18(data) {
        return regeneratorRuntime.wrap(function _callee18$(_context18) {
          while (1) {
            switch (_context18.prev = _context18.next) {
              case 0:
                _context18.t0 = cleanResponse;
                _context18.next = 3;
                return getOneMotivos(data);

              case 3:
                _context18.t1 = _context18.sent;
                return _context18.abrupt("return", (0, _context18.t0)(_context18.t1));

              case 5:
              case "end":
                return _context18.stop();
            }
          }
        }, _callee18);
      }));

      function motivosOne(_x18) {
        return _motivosOne.apply(this, arguments);
      }

      return motivosOne;
    }()
  }, {
    key: "subMotivosAll",
    value: function () {
      var _subMotivosAll = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee19(data) {
        return regeneratorRuntime.wrap(function _callee19$(_context19) {
          while (1) {
            switch (_context19.prev = _context19.next) {
              case 0:
                _context19.t0 = cleanResponse;
                _context19.next = 3;
                return getAllSubMotivos(data);

              case 3:
                _context19.t1 = _context19.sent;
                return _context19.abrupt("return", (0, _context19.t0)(_context19.t1));

              case 5:
              case "end":
                return _context19.stop();
            }
          }
        }, _callee19);
      }));

      function subMotivosAll(_x19) {
        return _subMotivosAll.apply(this, arguments);
      }

      return subMotivosAll;
    }()
  }, {
    key: "subMotivosOne",
    value: function () {
      var _subMotivosOne = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee20(data) {
        return regeneratorRuntime.wrap(function _callee20$(_context20) {
          while (1) {
            switch (_context20.prev = _context20.next) {
              case 0:
                _context20.t0 = cleanResponse;
                _context20.next = 3;
                return getOneSubMotivos(data);

              case 3:
                _context20.t1 = _context20.sent;
                return _context20.abrupt("return", (0, _context20.t0)(_context20.t1));

              case 5:
              case "end":
                return _context20.stop();
            }
          }
        }, _callee20);
      }));

      function subMotivosOne(_x20) {
        return _subMotivosOne.apply(this, arguments);
      }

      return subMotivosOne;
    }()
  }]);

  return Agente_tipificacion;
}();

exports.Agente_tipificacion = Agente_tipificacion;

var Agente_prioridad = /*#__PURE__*/function () {
  function Agente_prioridad() {
    _classCallCheck(this, Agente_prioridad);
  }

  _createClass(Agente_prioridad, [{
    key: "all",
    value: function () {
      var _all4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee21(data) {
        return regeneratorRuntime.wrap(function _callee21$(_context21) {
          while (1) {
            switch (_context21.prev = _context21.next) {
              case 0:
                _context21.t0 = cleanResponse;
                _context21.next = 3;
                return getAllPrioridad(data);

              case 3:
                _context21.t1 = _context21.sent;
                return _context21.abrupt("return", (0, _context21.t0)(_context21.t1));

              case 5:
              case "end":
                return _context21.stop();
            }
          }
        }, _callee21);
      }));

      function all(_x21) {
        return _all4.apply(this, arguments);
      }

      return all;
    }()
  }]);

  return Agente_prioridad;
}();

exports.Agente_prioridad = Agente_prioridad;

var Agente_estado = /*#__PURE__*/function () {
  function Agente_estado() {
    _classCallCheck(this, Agente_estado);
  }

  _createClass(Agente_estado, [{
    key: "all",
    value: function () {
      var _all5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee22(data) {
        return regeneratorRuntime.wrap(function _callee22$(_context22) {
          while (1) {
            switch (_context22.prev = _context22.next) {
              case 0:
                _context22.t0 = cleanResponse;
                _context22.next = 3;
                return getAllEstados(data);

              case 3:
                _context22.t1 = _context22.sent;
                return _context22.abrupt("return", (0, _context22.t0)(_context22.t1));

              case 5:
              case "end":
                return _context22.stop();
            }
          }
        }, _callee22);
      }));

      function all(_x22) {
        return _all5.apply(this, arguments);
      }

      return all;
    }()
  }]);

  return Agente_estado;
}();

exports.Agente_estado = Agente_estado;

var Agente_user = /*#__PURE__*/function () {
  function Agente_user() {
    _classCallCheck(this, Agente_user);
  }

  _createClass(Agente_user, [{
    key: "all",
    value: function () {
      var _all6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee23(data) {
        return regeneratorRuntime.wrap(function _callee23$(_context23) {
          while (1) {
            switch (_context23.prev = _context23.next) {
              case 0:
                _context23.t0 = cleanResponse;
                _context23.next = 3;
                return getAllUsuario(data);

              case 3:
                _context23.t1 = _context23.sent;
                return _context23.abrupt("return", (0, _context23.t0)(_context23.t1));

              case 5:
              case "end":
                return _context23.stop();
            }
          }
        }, _callee23);
      }));

      function all(_x23) {
        return _all6.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "one",
    value: function () {
      var _one2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee24(data) {
        return regeneratorRuntime.wrap(function _callee24$(_context24) {
          while (1) {
            switch (_context24.prev = _context24.next) {
              case 0:
                _context24.t0 = cleanResponse;
                _context24.next = 3;
                return getByUsuario(data);

              case 3:
                _context24.t1 = _context24.sent;
                return _context24.abrupt("return", (0, _context24.t0)(_context24.t1));

              case 5:
              case "end":
                return _context24.stop();
            }
          }
        }, _callee24);
      }));

      function one(_x24) {
        return _one2.apply(this, arguments);
      }

      return one;
    }()
  }, {
    key: "getdisponibles",
    value: function () {
      var _getdisponibles = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee25(data) {
        return regeneratorRuntime.wrap(function _callee25$(_context25) {
          while (1) {
            switch (_context25.prev = _context25.next) {
              case 0:
                _context25.t0 = cleanResponse;
                _context25.next = 3;
                return getUsuarioDisponible(data);

              case 3:
                _context25.t1 = _context25.sent;
                return _context25.abrupt("return", (0, _context25.t0)(_context25.t1));

              case 5:
              case "end":
                return _context25.stop();
            }
          }
        }, _callee25);
      }));

      function getdisponibles(_x25) {
        return _getdisponibles.apply(this, arguments);
      }

      return getdisponibles;
    }()
  }]);

  return Agente_user;
}();

exports.Agente_user = Agente_user;

var Agente_contacto = /*#__PURE__*/function () {
  function Agente_contacto() {
    _classCallCheck(this, Agente_contacto);
  }

  _createClass(Agente_contacto, [{
    key: "all",
    value: function () {
      var _all7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee26(data) {
        return regeneratorRuntime.wrap(function _callee26$(_context26) {
          while (1) {
            switch (_context26.prev = _context26.next) {
              case 0:
                _context26.t0 = cleanResponse;
                _context26.next = 3;
                return getAllContacto(data);

              case 3:
                _context26.t1 = _context26.sent;
                return _context26.abrupt("return", (0, _context26.t0)(_context26.t1));

              case 5:
              case "end":
                return _context26.stop();
            }
          }
        }, _callee26);
      }));

      function all(_x26) {
        return _all7.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "one",
    value: function () {
      var _one3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee27(data) {
        var method, id, contacto, APIS;
        return regeneratorRuntime.wrap(function _callee27$(_context27) {
          while (1) {
            switch (_context27.prev = _context27.next) {
              case 0:
                method = data.method, id = data.id;
                _context27.t0 = cleanResponse;
                _context27.next = 4;
                return getByContacto({
                  id: id
                });

              case 4:
                _context27.t1 = _context27.sent;
                contacto = (0, _context27.t0)(_context27.t1);

                if (!method) {
                  _context27.next = 19;
                  break;
                }

                _context27.next = 9;
                return getAPIs(data, contacto, 'contacto');

              case 9:
                APIS = _context27.sent;
                _context27.prev = 10;
                return _context27.abrupt("return", _objectSpread({
                  data: contacto
                }, APIS));

              case 14:
                _context27.prev = 14;
                _context27.t2 = _context27["catch"](10);
                console.log("error", _context27.t2);

              case 17:
                _context27.next = 20;
                break;

              case 19:
                return _context27.abrupt("return", contacto);

              case 20:
              case "end":
                return _context27.stop();
            }
          }
        }, _callee27, null, [[10, 14]]);
      }));

      function one(_x27) {
        return _one3.apply(this, arguments);
      }

      return one;
    }()
  }, {
    key: "add",
    value: function () {
      var _add4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee28(data) {
        return regeneratorRuntime.wrap(function _callee28$(_context28) {
          while (1) {
            switch (_context28.prev = _context28.next) {
              case 0:
                _context28.t0 = cleanResponse;
                _context28.next = 3;
                return addContacto(data);

              case 3:
                _context28.t1 = _context28.sent;
                return _context28.abrupt("return", (0, _context28.t0)(_context28.t1));

              case 5:
              case "end":
                return _context28.stop();
            }
          }
        }, _callee28);
      }));

      function add(_x28) {
        return _add4.apply(this, arguments);
      }

      return add;
    }()
  }, {
    key: "upd",
    value: function () {
      var _upd2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee29(data) {
        return regeneratorRuntime.wrap(function _callee29$(_context29) {
          while (1) {
            switch (_context29.prev = _context29.next) {
              case 0:
                _context29.t0 = cleanResponse;
                _context29.next = 3;
                return updContacto(data);

              case 3:
                _context29.t1 = _context29.sent;
                return _context29.abrupt("return", (0, _context29.t0)(_context29.t1));

              case 5:
              case "end":
                return _context29.stop();
            }
          }
        }, _callee29);
      }));

      function upd(_x29) {
        return _upd2.apply(this, arguments);
      }

      return upd;
    }()
  }]);

  return Agente_contacto;
}();

exports.Agente_contacto = Agente_contacto;

var Agente_sla = /*#__PURE__*/function () {
  function Agente_sla() {
    _classCallCheck(this, Agente_sla);
  }

  _createClass(Agente_sla, [{
    key: "all",
    value: function () {
      var _all8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee30(data) {
        return regeneratorRuntime.wrap(function _callee30$(_context30) {
          while (1) {
            switch (_context30.prev = _context30.next) {
              case 0:
                _context30.t0 = cleanResponse;
                _context30.next = 3;
                return getAllAcuerdo(data);

              case 3:
                _context30.t1 = _context30.sent;
                return _context30.abrupt("return", (0, _context30.t0)(_context30.t1));

              case 5:
              case "end":
                return _context30.stop();
            }
          }
        }, _callee30);
      }));

      function all(_x30) {
        return _all8.apply(this, arguments);
      }

      return all;
    }()
  }]);

  return Agente_sla;
}();

exports.Agente_sla = Agente_sla;

var Agente_grupo = /*#__PURE__*/function () {
  function Agente_grupo() {
    _classCallCheck(this, Agente_grupo);
  }

  _createClass(Agente_grupo, [{
    key: "all",
    value: function () {
      var _all9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee31(data) {
        return regeneratorRuntime.wrap(function _callee31$(_context31) {
          while (1) {
            switch (_context31.prev = _context31.next) {
              case 0:
                _context31.t0 = cleanResponse;
                _context31.next = 3;
                return getByGruposSkill(data);

              case 3:
                _context31.t1 = _context31.sent;
                return _context31.abrupt("return", (0, _context31.t0)(_context31.t1));

              case 5:
              case "end":
                return _context31.stop();
            }
          }
        }, _callee31);
      }));

      function all(_x31) {
        return _all9.apply(this, arguments);
      }

      return all;
    }()
  }]);

  return Agente_grupo;
}();

exports.Agente_grupo = Agente_grupo;

var Agente_cuenta = /*#__PURE__*/function () {
  function Agente_cuenta() {
    _classCallCheck(this, Agente_cuenta);
  }

  _createClass(Agente_cuenta, [{
    key: "all",
    value: function () {
      var _all10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee32(data) {
        return regeneratorRuntime.wrap(function _callee32$(_context32) {
          while (1) {
            switch (_context32.prev = _context32.next) {
              case 0:
                _context32.t0 = cleanResponse;
                _context32.next = 3;
                return getAllCuentas(data);

              case 3:
                _context32.t1 = _context32.sent;
                return _context32.abrupt("return", (0, _context32.t0)(_context32.t1));

              case 5:
              case "end":
                return _context32.stop();
            }
          }
        }, _callee32);
      }));

      function all(_x32) {
        return _all10.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "one",
    value: function () {
      var _one4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee33(data) {
        var method, id, cuenta, APIS;
        return regeneratorRuntime.wrap(function _callee33$(_context33) {
          while (1) {
            switch (_context33.prev = _context33.next) {
              case 0:
                method = data.method, id = data.id;
                _context33.t0 = cleanResponse;
                _context33.next = 4;
                return getByCuenta({
                  id: id
                });

              case 4:
                _context33.t1 = _context33.sent;
                cuenta = (0, _context33.t0)(_context33.t1);
                _context33.next = 8;
                return getAPIs(data, cuenta, 'cuenta');

              case 8:
                APIS = _context33.sent;

                if (!method) {
                  _context33.next = 19;
                  break;
                }

                _context33.prev = 10;
                return _context33.abrupt("return", _objectSpread({
                  data: cuenta
                }, APIS));

              case 14:
                _context33.prev = 14;
                _context33.t2 = _context33["catch"](10);
                console.log("error", _context33.t2);

              case 17:
                _context33.next = 20;
                break;

              case 19:
                return _context33.abrupt("return", cuenta);

              case 20:
              case "end":
                return _context33.stop();
            }
          }
        }, _callee33, null, [[10, 14]]);
      }));

      function one(_x33) {
        return _one4.apply(this, arguments);
      }

      return one;
    }()
  }, {
    key: "add",
    value: function () {
      var _add5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee34(data) {
        return regeneratorRuntime.wrap(function _callee34$(_context34) {
          while (1) {
            switch (_context34.prev = _context34.next) {
              case 0:
                _context34.t0 = cleanResponse;
                _context34.next = 3;
                return addCuenta(data);

              case 3:
                _context34.t1 = _context34.sent;
                return _context34.abrupt("return", (0, _context34.t0)(_context34.t1));

              case 5:
              case "end":
                return _context34.stop();
            }
          }
        }, _callee34);
      }));

      function add(_x34) {
        return _add5.apply(this, arguments);
      }

      return add;
    }()
  }, {
    key: "upd",
    value: function () {
      var _upd3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee35(data) {
        return regeneratorRuntime.wrap(function _callee35$(_context35) {
          while (1) {
            switch (_context35.prev = _context35.next) {
              case 0:
                _context35.t0 = cleanResponse;
                _context35.next = 3;
                return updCuenta(data);

              case 3:
                _context35.t1 = _context35.sent;
                return _context35.abrupt("return", (0, _context35.t0)(_context35.t1));

              case 5:
              case "end":
                return _context35.stop();
            }
          }
        }, _callee35);
      }));

      function upd(_x35) {
        return _upd3.apply(this, arguments);
      }

      return upd;
    }()
  }, {
    key: "getcontactsbyid",
    value: function () {
      var _getcontactsbyid = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee36(data) {
        return regeneratorRuntime.wrap(function _callee36$(_context36) {
          while (1) {
            switch (_context36.prev = _context36.next) {
              case 0:
                _context36.t0 = cleanResponse;
                _context36.next = 3;
                return getContacts(data);

              case 3:
                _context36.t1 = _context36.sent;
                return _context36.abrupt("return", (0, _context36.t0)(_context36.t1));

              case 5:
              case "end":
                return _context36.stop();
            }
          }
        }, _callee36);
      }));

      function getcontactsbyid(_x36) {
        return _getcontactsbyid.apply(this, arguments);
      }

      return getcontactsbyid;
    }()
  }]);

  return Agente_cuenta;
}();

exports.Agente_cuenta = Agente_cuenta;

var Agente_proceso = /*#__PURE__*/function () {
  function Agente_proceso() {
    _classCallCheck(this, Agente_proceso);
  }

  _createClass(Agente_proceso, [{
    key: "all",
    value: // aqui crear el next y back de procesos.
    function () {
      var _all11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee37(data) {
        return regeneratorRuntime.wrap(function _callee37$(_context37) {
          while (1) {
            switch (_context37.prev = _context37.next) {
              case 0:
                _context37.t0 = cleanResponse;
                _context37.next = 3;
                return getAllProcesos(data);

              case 3:
                _context37.t1 = _context37.sent;
                return _context37.abrupt("return", (0, _context37.t0)(_context37.t1));

              case 5:
              case "end":
                return _context37.stop();
            }
          }
        }, _callee37);
      }));

      function all(_x37) {
        return _all11.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "next",
    value: function () {
      var _next2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee38(data) {
        return regeneratorRuntime.wrap(function _callee38$(_context38) {
          while (1) {
            switch (_context38.prev = _context38.next) {
              case 0:
                _context38.t0 = cleanResponse;
                _context38.next = 3;
                return nextCase(data);

              case 3:
                _context38.t1 = _context38.sent;
                return _context38.abrupt("return", (0, _context38.t0)(_context38.t1));

              case 5:
              case "end":
                return _context38.stop();
            }
          }
        }, _callee38);
      }));

      function next(_x38) {
        return _next2.apply(this, arguments);
      }

      return next;
    }()
  }, {
    key: "back",
    value: function () {
      var _back = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee39(data) {
        return regeneratorRuntime.wrap(function _callee39$(_context39) {
          while (1) {
            switch (_context39.prev = _context39.next) {
              case 0:
                _context39.t0 = cleanResponse;
                _context39.next = 3;
                return backCase(data);

              case 3:
                _context39.t1 = _context39.sent;
                return _context39.abrupt("return", (0, _context39.t0)(_context39.t1));

              case 5:
              case "end":
                return _context39.stop();
            }
          }
        }, _callee39);
      }));

      function back(_x39) {
        return _back.apply(this, arguments);
      }

      return back;
    }()
  }, {
    key: "stages",
    value: function () {
      var _stages = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee40(data) {
        return regeneratorRuntime.wrap(function _callee40$(_context40) {
          while (1) {
            switch (_context40.prev = _context40.next) {
              case 0:
                _context40.t0 = cleanResponse;
                _context40.next = 3;
                return stagesCase(data);

              case 3:
                _context40.t1 = _context40.sent;
                return _context40.abrupt("return", (0, _context40.t0)(_context40.t1));

              case 5:
              case "end":
                return _context40.stop();
            }
          }
        }, _callee40);
      }));

      function stages(_x40) {
        return _stages.apply(this, arguments);
      }

      return stages;
    }()
  }]);

  return Agente_proceso;
}();

exports.Agente_proceso = Agente_proceso;

var Agente_check = /*#__PURE__*/function () {
  function Agente_check() {
    _classCallCheck(this, Agente_check);
  }

  _createClass(Agente_check, [{
    key: "all",
    value: function () {
      var _all12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee41(data) {
        return regeneratorRuntime.wrap(function _callee41$(_context41) {
          while (1) {
            switch (_context41.prev = _context41.next) {
              case 0:
                _context41.t0 = cleanResponse;
                _context41.next = 3;
                return getAllChecklist(data);

              case 3:
                _context41.t1 = _context41.sent;
                return _context41.abrupt("return", (0, _context41.t0)(_context41.t1));

              case 5:
              case "end":
                return _context41.stop();
            }
          }
        }, _callee41);
      }));

      function all(_x41) {
        return _all12.apply(this, arguments);
      }

      return all;
    }()
  }, {
    key: "check",
    value: function () {
      var _check = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee42(data) {
        var idcaso, idtarea, origin, value;
        return regeneratorRuntime.wrap(function _callee42$(_context42) {
          while (1) {
            switch (_context42.prev = _context42.next) {
              case 0:
                idcaso = data.idcaso, idtarea = data.idtarea, origin = data.origin, value = data.value;

                if (!(value === true || value === 1)) {
                  _context42.next = 9;
                  break;
                }

                _context42.t0 = cleanResponse;
                _context42.next = 5;
                return doneCheck({
                  idcaso: idcaso,
                  idtarea: idtarea,
                  origin: origin
                });

              case 5:
                _context42.t1 = _context42.sent;
                return _context42.abrupt("return", (0, _context42.t0)(_context42.t1));

              case 9:
                _context42.t2 = cleanResponse;
                _context42.next = 12;
                return undoneCheck({
                  idcaso: idcaso,
                  idtarea: idtarea,
                  origin: origin
                });

              case 12:
                _context42.t3 = _context42.sent;
                return _context42.abrupt("return", (0, _context42.t2)(_context42.t3));

              case 14:
              case "end":
                return _context42.stop();
            }
          }
        }, _callee42);
      }));

      function check(_x42) {
        return _check.apply(this, arguments);
      }

      return check;
    }()
  }, {
    key: "executescript",
    value: function () {
      var _executescript = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee43(data) {
        return regeneratorRuntime.wrap(function _callee43$(_context43) {
          while (1) {
            switch (_context43.prev = _context43.next) {
              case 0:
                _context43.t0 = cleanResponse;
                _context43.next = 3;
                return executescriptCheck(data);

              case 3:
                _context43.t1 = _context43.sent;
                return _context43.abrupt("return", (0, _context43.t0)(_context43.t1));

              case 5:
              case "end":
                return _context43.stop();
            }
          }
        }, _callee43);
      }));

      function executescript(_x43) {
        return _executescript.apply(this, arguments);
      }

      return executescript;
    }()
  }]);

  return Agente_check;
}();

exports.Agente_check = Agente_check;

var Agente_cf = /*#__PURE__*/function () {
  function Agente_cf() {
    _classCallCheck(this, Agente_cf);
  }

  _createClass(Agente_cf, [{
    key: "all",
    value: function () {
      var _all13 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee44(data) {
        return regeneratorRuntime.wrap(function _callee44$(_context44) {
          while (1) {
            switch (_context44.prev = _context44.next) {
              case 0:
                _context44.t0 = cleanResponse;
                _context44.next = 3;
                return getAllCfs(data);

              case 3:
                _context44.t1 = _context44.sent;
                return _context44.abrupt("return", (0, _context44.t0)(_context44.t1));

              case 5:
              case "end":
                return _context44.stop();
            }
          }
        }, _callee44);
      }));

      function all(_x44) {
        return _all13.apply(this, arguments);
      }

      return all;
    }()
  }]);

  return Agente_cf;
}();

exports.Agente_cf = Agente_cf;

var Agente_parametro = /*#__PURE__*/function () {
  function Agente_parametro() {
    _classCallCheck(this, Agente_parametro);
  }

  _createClass(Agente_parametro, [{
    key: "all",
    value: function () {
      var _all14 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee45(data) {
        return regeneratorRuntime.wrap(function _callee45$(_context45) {
          while (1) {
            switch (_context45.prev = _context45.next) {
              case 0:
                _context45.t0 = cleanResponse;
                _context45.next = 3;
                return getParametros(data);

              case 3:
                _context45.t1 = _context45.sent;
                return _context45.abrupt("return", (0, _context45.t0)(_context45.t1));

              case 5:
              case "end":
                return _context45.stop();
            }
          }
        }, _callee45);
      }));

      function all(_x45) {
        return _all14.apply(this, arguments);
      }

      return all;
    }()
  }]);

  return Agente_parametro;
}();

exports.Agente_parametro = Agente_parametro;

var Util = /*#__PURE__*/function () {
  function Util() {
    _classCallCheck(this, Util);
  }

  _createClass(Util, [{
    key: "encode",
    value: function encode(data) {
      return _encode(data);
    }
  }, {
    key: "decode",
    value: function decode(data) {
      return _decode(data);
    }
  }]);

  return Util;
}();

exports.Util = Util;