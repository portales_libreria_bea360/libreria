"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Integracion = exports.Parametro = exports.Archivo = exports.Nota = exports.Cuenta = exports.Caso = exports.Custom = exports.Auth = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var _require = require('../ContactMethods/indicadores/indicador'),
    getIndicador = _require.getIndicador;

var _require2 = require('../ContactMethods/auth/auth'),
    loginCp = _require2.loginCp,
    logoutCp = _require2.logoutCp,
    getByContact = _require2.getByContact,
    addContact = _require2.addContact,
    updContact = _require2.updContact,
    updatePass = _require2.updatePass,
    recoveryPass = _require2.recoveryPass;

var _require3 = require('../ContactMethods/casos/caso'),
    getAll = _require3.getAll,
    getBy = _require3.getBy,
    addCase = _require3.addCase,
    getProductos = _require3.getProductos,
    getMotivos = _require3.getMotivos,
    getSubMotivos = _require3.getSubMotivos;

var _require4 = require('../ContactMethods/cuentas/cuenta'),
    getAllCuenta = _require4.getAllCuenta;

var _require5 = require('../ContactMethods/notas/nota'),
    getNotes = _require5.getNotes,
    addNote = _require5.addNote;

var _require6 = require('../ContactMethods/parametros/parametro'),
    getParametros = _require6.getParametros;

var _require7 = require('../ContactMethods/archivos/archivo'),
    getArchivos = _require7.getArchivos,
    upload = _require7.upload,
    downLoad = _require7.downLoad,
    destroyArchivo = _require7.destroyArchivo;

var _require8 = require('../util/axios'),
    axiosCustom = _require8.axiosCustom;

var _require9 = require('../ContactMethods/integraciones/integracion'),
    serviceRest = _require9.serviceRest;

var _require10 = require('../util/instance'),
    Instances = _require10.Instances;

var Auth = /*#__PURE__*/function () {
  function Auth() {
    _classCallCheck(this, Auth);
  }

  _createClass(Auth, [{
    key: "login",
    value: function login(data) {
      return loginCp(data);
    }
  }, {
    key: "logout",
    value: function logout(data) {
      return logoutCp(data);
    }
  }, {
    key: "getBy",
    value: function getBy(data) {
      return getByContact(data);
    }
  }, {
    key: "one",
    value: function one(value) {
      var instance = new Instances();

      switch (value) {
        case 'contacto':
          return instance.getContacto().contacto;

        case 'company':
          return instance.getContacto().company;

        default:
          return instance.getContacto();
      }
    }
  }, {
    key: "add",
    value: function add(data) {
      return addContact(data);
    }
  }, {
    key: "upd",
    value: function upd(data) {
      return updContact(data);
    }
  }, {
    key: "updPass",
    value: function updPass(data) {
      return updatePass(data);
    }
  }, {
    key: "recovery",
    value: function recovery(data) {
      return recoveryPass(data);
    }
  }]);

  return Auth;
}();

exports.Auth = Auth;

var Custom = /*#__PURE__*/function () {
  function Custom() {
    _classCallCheck(this, Custom);
  }

  _createClass(Custom, [{
    key: "service",
    value: function service(data) {
      return axiosCustom(data);
    }
  }]);

  return Custom;
}();

exports.Custom = Custom;

var Caso = /*#__PURE__*/function () {
  function Caso() {
    _classCallCheck(this, Caso);
  }

  _createClass(Caso, [{
    key: "all",
    value: function all(data) {
      return getAll(data);
    }
  }, {
    key: "one",
    value: function one(data) {
      return getBy(data);
    }
  }, {
    key: "add",
    value: function add(data) {
      return addCase(data);
    }
  }, {
    key: "indicadores",
    value: function indicadores(data) {
      return getIndicador(data);
    }
  }, {
    key: "productos",
    value: function productos(data) {
      return getProductos(data);
    }
  }, {
    key: "motivos",
    value: function motivos(data) {
      return getMotivos(data);
    }
  }, {
    key: "subMotivos",
    value: function subMotivos(data) {
      return getSubMotivos(data);
    }
  }]);

  return Caso;
}();

exports.Caso = Caso;

var Cuenta = /*#__PURE__*/function () {
  function Cuenta() {
    _classCallCheck(this, Cuenta);
  }

  _createClass(Cuenta, [{
    key: "all",
    value: function all(data) {
      return getAllCuenta(data);
    }
  }]);

  return Cuenta;
}();

exports.Cuenta = Cuenta;

var Nota = /*#__PURE__*/function () {
  function Nota() {
    _classCallCheck(this, Nota);
  }

  _createClass(Nota, [{
    key: "all",
    value: function all(data) {
      return getNotes(data);
    }
  }, {
    key: "add",
    value: function add(data) {
      return addNote(data);
    }
  }]);

  return Nota;
}();

exports.Nota = Nota;

var Archivo = /*#__PURE__*/function () {
  function Archivo() {
    _classCallCheck(this, Archivo);
  }

  _createClass(Archivo, [{
    key: "all",
    value: function all(data) {
      return getArchivos(data);
    }
  }, {
    key: "add",
    value: function add(data) {
      return upload(data);
    }
  }, {
    key: "download",
    value: function download(data) {
      return downLoad(data);
    }
  }, {
    key: "destroy",
    value: function destroy(data) {
      return destroyArchivo(data);
    }
  }]);

  return Archivo;
}();

exports.Archivo = Archivo;

var Parametro = /*#__PURE__*/function () {
  function Parametro() {
    _classCallCheck(this, Parametro);
  }

  _createClass(Parametro, [{
    key: "all",
    value: function all(data) {
      return getParametros(data);
    }
  }]);

  return Parametro;
}();

exports.Parametro = Parametro;

var Integracion = /*#__PURE__*/function () {
  function Integracion() {
    _classCallCheck(this, Integracion);
  }

  _createClass(Integracion, [{
    key: "rest",
    value: function rest(data) {
      return serviceRest(data);
    }
  }, {
    key: "iframe",
    value: function iframe(data) {
      return serviceIframe(data);
    }
  }, {
    key: "addRest",
    value: function addRest(data) {
      return serviceAddRest(data);
    }
  }, {
    key: "addIframe",
    value: function addIframe(data) {
      return serviceAddIframe(data);
    }
  }]);

  return Integracion;
}();

exports.Integracion = Integracion;