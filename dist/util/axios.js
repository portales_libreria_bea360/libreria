"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.axiosBea360 = axiosBea360;
exports.axiosBea360Register = axiosBea360Register;
exports.axiosCustom = axiosCustom;
exports.axiosIntegration = axiosIntegration;
exports.axiosUser = axiosUser;

/**Metodo base axios que recibe un objeto con dos variables
 * casos.url(base de datos del cliente) y casos.key(variable encriptada)
 * tambien recibe otro parametro que es para completar el resto de url
 * cuando un contacto se esta registrando se genera un token con con el company/anonymous:anonymous
*/
var axios = require('axios');

var _require = require('./methods'),
    encode = _require.encode;

var _require2 = require('./instance'),
    Instances = _require2.Instances;

function axiosBea360(url, data) {
  var con = new Instances();

  if (typeof con.getContacto() != 'undefined' && Object.keys(con.getContacto()).length !== 0) {
    return axios.create({
      baseURL: "".concat(con.getContacto().url, "/apir/v10/cp/").concat(url),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic ".concat(encode(con.getContacto(), "token"))
      }
    });
  }
}

function axiosBea360Register(data, url) {
  return axios.create({
    baseURL: "".concat(data.url, "/apir/v10/cp/").concat(url),
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Basic ".concat(encode(data, "anonymous"))
    }
  });
} //metodos custom con el portal bea360


function axiosCustom(data) {
  var instance = axiosBea360(data.endpoint);
  return instance[data.method]('', data.dataToSend);
}

function axiosIntegration(_ref) {
  var baseURL = _ref.baseURL,
      action = _ref.action;
  var instance = axios.create({
    baseURL: "".concat(baseURL, "/").concat(action.path),
    headers: {
      "Content-Type": "application/json",
      "x-rapidapi-host": "".concat(action.key1),
      "x-rapidapi-key": "".concat(action.key2)
    }
  });
  return instance;
} //Axios agente bea360


function axiosUser(url, data) {
  var con = new Instances(); //si el token no existe

  if (typeof data.token === 'undefined') {
    //entra por navegador
    var instance = axios.create({
      baseURL: "".concat(con.getUser().url, "/apir/v11/").concat(url),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic ".concat(encode(con.getUser(), "userEncode"))
      }
    });
    return instance;
  } else {
    /**
     * entra por servidor
     * en la data viene un token
     */
    var _instance = axios.create({
      baseURL: "".concat(data.url, "/apir/v11/").concat(url),
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic ".concat(encode(data, "userEncodeServe"))
      }
    });

    return _instance;
  }
}