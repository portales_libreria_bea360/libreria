"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Instances = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var jwt = require('jsonwebtoken');

var CryptoJS = require('crypto-js/crypto-js');
/**
 * metodos para crear instancia de Usuario
 * token: res.data.data.token,
    permisos: res.data.data.permisos,
    usuario: res.data.data.usuario,
    url: url,
    company:company
 */


var Instances = /*#__PURE__*/function () {
  function Instances() {
    _classCallCheck(this, Instances);
  }

  _createClass(Instances, [{
    key: "createUser",
    value: function createUser(data) {
      var Object = {};
      var token = data.token,
          permisos = data.permisos,
          usuario = data.usuario,
          url = data.url,
          company = data.company; // normal encript

      if (data) {
        localStorage.setItem('USERINSTANCE', JSON.stringify(encryptAES(data, "abcdefgabcdefg12")));
      } // full encript 
      // for (let i in data){
      //     Object[encryptAES(i, "abcdefgabcdefg12")] = encryptAES(data[i],"abcdefgabcdefg12")
      // }
      // if (data) {
      //     localStorage.setItem('USERINSTANCE', JSON.stringify(encryptAES(Object, "abcdefgabcdefg13")))
      // }

    }
  }, {
    key: "getUser",
    value: function getUser(value) {
      var Object = {}; // normal decript

      var data = JSON.parse(decrypAES(JSON.parse(localStorage.getItem('USERINSTANCE')), "abcdefgabcdefg12"));
      var token = data.token,
          permisos = data.permisos,
          usuario = data.usuario,
          url = data.url,
          company = data.company;

      if (value) {
        return {
          usuario: data.usuario,
          permisos: data.permisos,
          company: data.company
        };
      } else {
        return data;
      } // let data = JSON.parse(decrypAES(JSON.parse(localStorage.getItem('USERINSTANCE')), "abcdefgabcdefg13"))
      // let { token, permisos, usuario, url, company } = data
      // for (let i in data){
      //     Object[JSON.parse(decrypAES(i, "abcdefgabcdefg12"))] = JSON.parse(decrypAES(data[i], "abcdefgabcdefg12"))
      // }
      // if (value) {
      //     return {
      //         usuario: Object.usuario,
      //         permisos: Object.permisos,
      //         company: Object.company
      //     }
      // } else {
      //     return Object
      // }

    } //metodos de portal contacto

    /** variables encriptadas
     *  contacto: response.data.contacto,
        company: data.company,
        url: data.url
     */

  }, {
    key: "createContacto",
    value: function createContacto(data) {
      if (data) {
        var token = jwt.sign(data, 'clave');
        localStorage.setItem('INSTANCE', JSON.stringify(token));
      }
    }
  }, {
    key: "getContacto",
    value: function getContacto() {
      return jwt.verify(JSON.parse(localStorage.getItem('INSTANCE')), 'clave', function (err, decoded) {
        return decoded;
      });
    }
  }, {
    key: "deleteContacto",
    value: function deleteContacto(data) {
      if (data) {
        var _data = jwt.verify(JSON.parse(localStorage.getItem('INSTANCE')), 'clave', function (err, decoded) {
          var objeto = {};

          for (var i in decoded) {
            if (i === 'contacto') {
              objeto[i] = decoded[i];
            }
          }

          return objeto;
        });

        var token = jwt.sign(_data, 'clave');
        localStorage.setItem('INSTANCE', JSON.stringify(token));
      } else {
        //deslogueo sin guardar nada
        localStorage.removeItem('INSTANCE');
      }
    }
  }, {
    key: "getInstance",
    value: function getInstance() {
      var data = JSON.parse(localStorage.getItem('INSTANCESERVER'));
      return data;
    }
  }]);

  return Instances;
}();

exports.Instances = Instances;

function encryptAES(data, llave) {
  var key = CryptoJS.enc.Utf8.parse(llave);
  var srcs = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
  return CryptoJS.AES.encrypt(srcs, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  }).toString();
}

function decrypAES(data, llave) {
  var key = CryptoJS.enc.Utf8.parse(llave);
  var decrypt = CryptoJS.AES.decrypt(data, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  return CryptoJS.enc.Utf8.stringify(decrypt).toString();
}