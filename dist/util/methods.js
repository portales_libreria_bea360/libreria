"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cleanData = cleanData;
exports.cleanDataRecovery = cleanDataRecovery;
exports.assignData = assignData;
exports.encode = encode;
exports.decode = decode;
exports.cleanResponse = cleanResponse;
exports.getAPIs = getAPIs;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _get = require('lodash/get');

var _require = require('../lib/agenteBea360'),
    Agente_cf = _require.Agente_cf,
    Agente_sla = _require.Agente_sla,
    Agente_prioridad = _require.Agente_prioridad,
    Agente_estado = _require.Agente_estado,
    Agente_check = _require.Agente_check,
    Agente_proceso = _require.Agente_proceso,
    Agente_cuenta = _require.Agente_cuenta,
    Agente_contacto = _require.Agente_contacto,
    Agente_grupo = _require.Agente_grupo,
    Agente_user = _require.Agente_user,
    Agente_tipificacion = _require.Agente_tipificacion;

function cleanData(data) {
  var params = {}; //codigo que extrae la key y url del objeto principal

  for (var i in data) {
    if (i != 'url' && i != 'company' && i != 'token') {
      params[i] = data[i];
    }
  }

  return params;
}

function cleanDataRecovery(data) {
  var params = {}; //codigo que extrae la key y url del objeto principal

  for (var i in data) {
    if (i != 'url' && i != 'token') {
      params[i] = data[i];
    }
  }

  return params;
}
/**
 * metodo que se encarga de asignar un valor a la data que se envia desde el front
 */


function assignData(data) {
  return typeof data === 'undefined' ? {} : data;
}
/**metodos de encriptacion de credenciales de usuario, en este se encripta en base64
 * la company, usuario y contraseña para que despues utilizarlo como cabecera en las llamadas
 * a los otros metodos de la API (casos, contactos y cuentas)
 */


function encode(key, condicion) {
  switch (condicion) {
    case 'auth':
      return new Buffer.from("".concat(key.company, "/").concat(key.email, ":").concat(key.pass || 'anonymous')).toString('base64');

    case 'token':
      return new Buffer.from("".concat(key.company, "/").concat(key.contacto.email[0], ":").concat(key.contacto.pass || 'anonymous')).toString('base64');

    case 'anonymous':
      return new Buffer.from("".concat(key.company, "/").concat(condicion, ":").concat(condicion)).toString('base64');

    case 'userEncode':
      //encode de agente
      return new Buffer.from("".concat(key.company, "/").concat(key.usuario.user, ":").concat(key.token)).toString('base64');

    case 'userEncodeServe':
      //entra por servidor
      return new Buffer.from("".concat(key.company, "/").concat(key.usuario, ":").concat(key.token)).toString('base64');

    default:
      return new Buffer.from(JSON.stringify(key)).toString('base64');
  }
}
/**Metodo para limpiar y extraer la data de las respuestas */


function cleanResponse(response) {
  var data = _get(response, 'data.data', {});

  return data;
}
/**metodo para desencriptar variables en base64 */


function decode(key) {
  var buff = new Buffer.from(key, 'base64');
  var decodeBase64Key = buff.toString('ascii');
  return decodeBase64Key;
}

function getAPIs(_x, _x2, _x3) {
  return _getAPIs.apply(this, arguments);
}

function _getAPIs() {
  _getAPIs = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(data, objeto, type) {
    var method, id, Object, ArrayService, arr, tip, services, indice, functionSer, cont, i;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            method = data.method, id = data.id;
            Object = {};
            ArrayService = [];
            arr = ['acuerdos', 'cfs', 'prioridades', 'estados', 'checklist', 'contactos', 'cuentas', 'tipificacion', 'procesos', 'pasos', 'usuarios', 'grupos'];
            tip = ['productos', 'motivos', 'submotivos'];
            services = method.filter(function (a) {
              return arr.find(function (b) {
                return b === a;
              });
            }); // filtro solo los que corresponden
            //reemplazo tipificación por el otro array.

            if (services.includes('tipificacion')) {
              indice = services.indexOf('tipificacion');
              services.splice.apply(services, [indice, 1].concat(tip));
            }

            functionSer = services.map(function (ser) {
              return _objectSpread({
                name: ser
              }, nameServicio(ser, objeto, type));
            });
            _context.next = 10;
            return Promise.allSettled(functionSer.map(function (a) {
              return a.objeto[a["function"]](a.data);
            }));

          case 10:
            ArrayService = _context.sent;
            cont = 0;
            _context.t0 = regeneratorRuntime.keys(ArrayService);

          case 13:
            if ((_context.t1 = _context.t0()).done) {
              _context.next = 29;
              break;
            }

            i = _context.t1.value;

            if (!(services[cont] === 'productos' || services[cont] === 'motivos' || services[cont] === 'submotivos')) {
              _context.next = 19;
              break;
            }

            Object[services[cont]] = [ArrayService[i].value];
            _context.next = 26;
            break;

          case 19:
            if (!(services[cont] === 'contactos' || services[cont] === 'cuentas' || services[cont] === 'usuarios')) {
              _context.next = 25;
              break;
            }

            _context.next = 22;
            return addObject(ArrayService[i].value, objeto, services[cont]);

          case 22:
            Object[services[cont]] = _context.sent;
            _context.next = 26;
            break;

          case 25:
            Object[services[cont]] = ArrayService[i].value;

          case 26:
            cont++;
            _context.next = 13;
            break;

          case 29:
            return _context.abrupt("return", Object);

          case 30:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getAPIs.apply(this, arguments);
}

function addObject(_x4, _x5, _x6) {
  return _addObject.apply(this, arguments);
}

function _addObject() {
  _addObject = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(allData, objeto, type) {
    var contacto, match, oneContacto, cuenta, _match, oneCuenta, usuario, _match2, oneUser;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (!(type === 'contactos')) {
              _context2.next = 13;
              break;
            }

            contacto = new Agente_contacto();
            match = allData.find(function (obj) {
              return obj.id === objeto.idcontacto;
            }); // busco que si el contacto del caso esta en los primeros 10 resultados de la API de contactosAll

            if (!(!match && objeto.idcontacto !== null)) {
              _context2.next = 10;
              break;
            }

            _context2.next = 6;
            return contacto.one({
              id: objeto.idcontacto
            });

          case 6:
            oneContacto = _context2.sent;
            return _context2.abrupt("return", allData.concat(oneContacto));

          case 10:
            return _context2.abrupt("return", allData);

          case 11:
            _context2.next = 37;
            break;

          case 13:
            if (!(type === 'cuentas')) {
              _context2.next = 26;
              break;
            }

            cuenta = new Agente_cuenta();
            _match = allData.find(function (obj) {
              return obj.id === objeto.idcuenta;
            }); // busco que si el contacto del caso esta en los primeros 10 resultados de la API de contactosAll

            if (!(!_match && objeto.idcuenta !== null)) {
              _context2.next = 23;
              break;
            }

            _context2.next = 19;
            return cuenta.one({
              id: objeto.idcuenta
            });

          case 19:
            oneCuenta = _context2.sent;
            return _context2.abrupt("return", allData.concat(oneCuenta));

          case 23:
            return _context2.abrupt("return", allData);

          case 24:
            _context2.next = 37;
            break;

          case 26:
            if (!(type === 'usuarios')) {
              _context2.next = 37;
              break;
            }

            usuario = new Agente_user();
            _match2 = allData.find(function (user) {
              return user.id === objeto.idusuarioasignado;
            });

            if (!(!_match2 && objeto.idusuarioasignado !== null)) {
              _context2.next = 36;
              break;
            }

            _context2.next = 32;
            return usuario.one({
              id: objeto.idusuarioasignado
            });

          case 32:
            oneUser = _context2.sent;
            return _context2.abrupt("return", allData.concat(oneUser));

          case 36:
            return _context2.abrupt("return", allData);

          case 37:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _addObject.apply(this, arguments);
}

function nameServicio(value, objeto, type) {
  var acuerdo = new Agente_sla();
  var cf = new Agente_cf();
  var prioridad = new Agente_prioridad();
  var estado = new Agente_estado();
  var check = new Agente_check();
  var contacto = new Agente_contacto();
  var tip = new Agente_tipificacion();
  var proceso = new Agente_proceso();
  var cuenta = new Agente_cuenta();
  var grupo = new Agente_grupo();
  var usuario = new Agente_user();

  switch (value) {
    case 'acuerdos':
      return {
        objeto: acuerdo,
        "function": 'all'
      };

    case 'cfs':
      return {
        objeto: cf,
        "function": 'all',
        data: {
          idtipoobjeto: typeObjectCf(type)
        }
      };

    case 'prioridades':
      return {
        objeto: prioridad,
        "function": 'all'
      };

    case 'estados':
      return {
        objeto: estado,
        "function": 'all'
      };

    case 'checklist':
      return {
        objeto: check,
        "function": 'all',
        data: {
          idcaso: objeto.id // id del caso

        }
      };

    case 'contactos':
      return {
        objeto: contacto,
        "function": 'all',
        data: {
          pagina: 1,
          cantidad: 10
        }
      };

    case 'productos':
      return {
        objeto: tip,
        "function": 'productosOne',
        data: {
          id: objeto.idproducto
        }
      };

    case 'motivos':
      return {
        objeto: tip,
        "function": 'motivosOne',
        data: {
          id: objeto.idtipo
        }
      };

    case 'submotivos':
      return {
        objeto: tip,
        "function": 'subMotivosOne',
        data: {
          id: objeto.idsubtipo
        }
      };

    case 'procesos':
      return {
        objeto: proceso,
        "function": 'all',
        data: {
          activo: 1,
          cantidad: 10,
          pagina: 1
        }
      };

    case 'cuentas':
      return {
        objeto: cuenta,
        "function": 'all',
        data: {
          idcontacto: objeto.idcontacto,
          cantidad: 10,
          pagina: 1
        }
      };

    case 'pasos':
      return {
        objeto: proceso,
        "function": 'stages',
        data: {
          id: objeto.id
        }
      };

    case 'usuarios':
      return {
        objeto: usuario,
        "function": 'getdisponibles',
        data: {
          pagina: 1,
          cantidad: 10
        }
      };

    case 'grupos':
      return {
        objeto: grupo,
        "function": 'all',
        data: {
          tipo: 2
        }
      };
  }
}

function typeObjectCf(type) {
  switch (type) {
    case 'caso':
      return 1;

    case 'contacto':
      return 2;

    case 'cuenta':
      return 3;
  }
} // for(let i in method){
//     if (method[i] === 'acuerdos') {
//         const acuerdo = new Agente_sla()
//         Object[method[i]] = await acuerdo.all()
//     } else if (method[i] === 'cfs') {
//         const cf = new Agente_cf()
//         Object[method[i]] = await cf.all({ idtipoobjeto: typeObjectCf() })
//     }   else if (method[i] === 'prioridades') {
//         const prioridad = new Agente_prioridad() 
//         Object[method[i]] = await prioridad.all()
//     } else if (method[i] === 'estados'){
//         const estado = new Agente_estado()
//         Object[method[i]] = await estado.all()
//     } else if (method[i] === 'checklist') {
//         const check = new Agente_check()
//         Object[method[i]] = await check.all({
//             idcaso: id
//         })
//     } 
//     else if (method[i] === 'grupos') {
//         const grupo = new Agente_grupo()
//          Object[method[i]] = await grupo.all({
//             tipo: 2 // 1 = usuarios, 2 = contactos
//         })
//     }
//     // desde aqui no se va a mostrar todavia
//     else if (method[i] === 'contactos') {
//         const contacto = new Agente_contacto()
//         const allContactos = await contacto.all({ idcuenta: 0 }) // traigo todos los contactos
//         let match = allContactos.find(contact => contact.id === objeto.idcontacto) // busco que si el contacto del caso esta en los primeros 10 resultados de la API de contactosAll
//         if (!match && objeto.idcontacto !== null) {
//             const oneContacto = await contacto.one({ id: objeto.idcontacto })
//             Object[method[i]] = allContactos.concat(oneContacto)  // uno los resultados
//         } else {
//             Object[method[i]] = allContactos
//         }
//     } else if (method[i] === 'tipificacion') {
//         const tip = new Agente_tipificacion()
//         const productos = [await tip.productosOne({ id: objeto.idproducto })]
//         const motivos = [await tip.motivosOne({id: objeto.idtipo})]
//         const subMotivos = [await tip.subMotivosOne({id: objeto.idsubtipo })]
//         Object['productos'] = productos
//         Object['motivos'] = motivos
//         Object['submotivos'] = subMotivos
//     } else if (method[i] === 'cuentas') {
//         const cuenta = new Agente_cuenta()
//         const allCuentas = await cuenta.all({ pagina: 1, cantidad: 10 }) 
//         let match = allCuentas.find(cuent => cuent.id === objeto.idcuenta)
//         if (!match && objeto.idcuenta !== null) {
//             const oneCuenta = await cuenta.one({ id: objeto.idcuenta })
//             Object[method[i]] = allCuentas.concat(oneCuenta)  // uno los resultados
//         } else {
//             Object[method[i]] = allCuentas
//         }
//     } else if (method[i] === 'procesos') {
//         const proceso = new Agente_proceso()
//         const allProcesos = await proceso.all({ activo: 1, cantidad: 10, pagina: 1 })
//         Object[method[i]] = allProcesos
//     } else if (method[i] === 'pasos') {
//         const proceso = new Agente_proceso()
//         const allStages = await proceso.stages({
//              id: objeto.id
//         })
//         Object[method[i]] = allStages
//     } else if (method[i] ==='usuarios') {
//         // hacer logica para cuenado el usuario no esta entre los prineros 10
//         const usuario = new Agente_user()
//         const allUsuarios = await usuario.getdisponibles({ pagina: 1, cantidad: 10 })
//         let match = allUsuarios.find(user => user.id === objeto.idusuarioasignado)
//         if (!match && objeto.idusuarioasignado !== null) {
//             const oneUser = await usuario.one({ id: objeto.idusuarioasignado })
//             Object[method[i]] = allUsuarios.concat(oneUser)  // uno los resultados
//         } else {
//             Object[method[i]] = allUsuarios
//         }
//     }
// }
// return Object