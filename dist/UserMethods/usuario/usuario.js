"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllUsuario = getAllUsuario;
exports.getByUsuario = getByUsuario;
exports.getUsuarioDisponible = getUsuarioDisponible;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllUsuario(data) {
  var url = "usuario/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getByUsuario(data) {
  var url = "usuario/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getUsuarioDisponible(data) {
  var url = "usuario/getdisponibles";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}