"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllEstados = getAllEstados;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllEstados(data) {
  var url = "estado/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get();
}