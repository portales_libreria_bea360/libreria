"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAll = getAll;
exports.getBy = getBy;
exports.addCase = addCase;
exports.updCase = updCase;
exports.asignadoGrupo = asignadoGrupo;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    cleanData = _require2.cleanData,
    assignData = _require2.assignData;

function getAll(data) {
  var url = "caso/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  }); //variables limpias para hacer peticion
}

function getBy(data) {
  var url = "caso/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function asignadoGrupo(data) {
  var url = "caso/asignadoagrupo/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function updCase(data) {
  var url = "caso/upd";
  var instance = axiosUser(url, assignData(data));
  return instance.put('', data);
}

function addCase(data) {
  var url = "caso/add";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}