"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllChecklist = getAllChecklist;
exports.executescriptCheck = executescriptCheck;
exports.doneCheck = doneCheck;
exports.undoneCheck = undoneCheck;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllChecklist(data) {
  var url = "checklist/tareas/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function executescriptCheck(data) {
  var url = "checklist/tareas/executescript";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
} // estyos metodos se accionan con los check pero pertenecen casos


function doneCheck(data) {
  var url = "caso/done";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}

function undoneCheck(data) {
  var url = "caso/undone";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}