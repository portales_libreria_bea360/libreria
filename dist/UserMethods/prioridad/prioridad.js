"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllPrioridad = getAllPrioridad;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllPrioridad(data) {
  var url = "prioridad/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get();
}