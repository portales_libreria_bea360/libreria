"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllNotas = getAllNotas;
exports.addNotas = addNotas;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllNotas(data) {
  var url = "actividad/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function addNotas(data) {
  var url = "actividad/add";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}