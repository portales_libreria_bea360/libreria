"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getParametros = getParametros;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getParametros(data) {
  var url = "prparametros/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}