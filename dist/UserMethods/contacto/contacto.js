"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllContacto = getAllContacto;
exports.getByContacto = getByContacto;
exports.addContacto = addContacto;
exports.updContacto = updContacto;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllContacto(data) {
  var url = "contacto/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getByContacto(data) {
  var url = "contacto/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function addContacto(data) {
  var url = "contacto/add";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}

function updContacto(data) {
  var url = "contacto/upd";
  var instance = axiosUser(url, assignData(data));
  return instance.put('', data);
}