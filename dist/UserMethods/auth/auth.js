"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.userToken = userToken;
exports.userLogin = userLogin;
exports.authwithtoken = authwithtoken;

var axios = require('axios');

var _require = require('../../util/instance'),
    Instances = _require.Instances;

function userToken(data) {
  var id = data.id,
      company = data.company,
      url = data.url;
  var instance = axios.create({
    baseURL: "".concat(url, "/apir/v11/login/auth/tokenvalidate"),
    headers: {
      "Content-Type": "application/json"
    }
  });
  return instance.post('', {
    id: id,
    company: company
  }).then(function (res) {
    if (res.data.data) {
      var _instance = new Instances();

      _instance.createUser({
        token: res.data.data.token,
        permisos: res.data.data.permisos,
        usuario: res.data.data.usuario,
        url: url,
        company: company
      });

      return {
        ok: true
      };
    }
  });
} //metodo 


function userLogin(data) {
  var user = data.user,
      pass = data.pass,
      company = data.company,
      url = data.url,
      local = data.local,
      type = data.type;
  var instance = axios.create({
    baseURL: "".concat(url, "/apir/v11/login/auth"),
    headers: {
      "Content-Type": "application/json"
    }
  });
  return instance.post('', {
    user: user,
    pass: pass,
    company: company
  }).then(function (res) {
    if (res.data) {
      //para ocupar la libreria en un backend NODE
      if (type === 'back') {
        return {
          token: res.data.token,
          permisos: res.data.permisos,
          usuario: res.data.usuario,
          url: url,
          company: company,
          type: type
        };
      } else {
        // si es para front, la libreria se encarga de crear y guardar los datos en localStorage
        var con = new Instances();
        con.createUser({
          token: res.data.token,
          permisos: res.data.permisos,
          usuario: res.data.usuario,
          url: url,
          company: company
        });
        return res.data;
      }
    }
  });
}

function authwithtoken(data) {
  var url = data.url,
      token = data.token,
      company = data.company;
  var instance = axios.create({
    baseURL: "".concat(url, "/apir/v11/login/authwithtoken"),
    headers: {
      "Content-Type": "application/json"
    }
  });
  return instance.post('', {
    token: token
  }).then(function (res) {
    if (res.data) {
      // si es para front, la libreria se encarga de crear y guardar los datos en localStorage
      var con = new Instances();
      con.createUser({
        token: res.data.token,
        permisos: res.data.permisos,
        usuario: res.data.usuario,
        url: url,
        company: company
      });
      return res.data;
    }
  });
}