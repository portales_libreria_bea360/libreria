"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllProcesos = getAllProcesos;
exports.nextCase = nextCase;
exports.backCase = backCase;
exports.stagesCase = stagesCase;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllProcesos(data) {
  var url = "workflow/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function nextCase(data) {
  var url = "workflow/next";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}

function backCase(data) {
  var url = "workflow/back";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}

function stagesCase(data) {
  var url = "workflow/stages";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}