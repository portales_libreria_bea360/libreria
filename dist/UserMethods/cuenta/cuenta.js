"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllCuentas = getAllCuentas;
exports.getByCuenta = getByCuenta;
exports.addCuenta = addCuenta;
exports.updCuenta = updCuenta;
exports.getContacts = getContacts;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllCuentas(data) {
  var url = "cuenta/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getByCuenta(data) {
  var url = "cuenta/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function addCuenta(data) {
  var url = "cuenta/add";
  var instance = axiosUser(url, assignData(data));
  return instance.post('', data);
}

function updCuenta(data) {
  var url = "cuenta/upd";
  var instance = axiosUser(url, assignData(data));
  return instance.put('', data);
}

function getContacts(data) {
  var url = "cuenta/getcontactsbyid";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}