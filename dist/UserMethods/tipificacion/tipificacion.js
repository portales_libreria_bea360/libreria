"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllProductos = getAllProductos;
exports.getAllMotivos = getAllMotivos;
exports.getAllSubMotivos = getAllSubMotivos;
exports.getOneProductos = getOneProductos;
exports.getOneMotivos = getOneMotivos;
exports.getOneSubMotivos = getOneSubMotivos;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var _require2 = require('../../util/methods'),
    assignData = _require2.assignData;

function getAllProductos(data) {
  var url = "producto/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getOneProductos(data) {
  var url = "producto/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getAllMotivos(data) {
  var url = "tipo/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getOneMotivos(data) {
  var url = "tipo/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getAllSubMotivos(data) {
  var url = "subtipo/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function getOneSubMotivos(data) {
  var url = "subtipo/getby";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}