"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllArchivos = getAllArchivos;
exports.addArchivos = addArchivos;
exports.downLoad = downLoad;
exports.destroyArchivo = destroyArchivo;

var _require = require('../../util/axios'),
    axiosUser = _require.axiosUser;

var axios = require('axios');

var _require2 = require('file-saver'),
    saveAs = _require2.saveAs;

var get = require('lodash/get');

var _require3 = require('../../util/methods'),
    assignData = _require3.assignData;

var _require4 = require('../../util/instance'),
    Instances = _require4.Instances;

var _require5 = require('../../util/methods'),
    encode = _require5.encode;

var con = new Instances();

function getAllArchivos(data) {
  var url = "archivos/get";
  var instance = axiosUser(url, assignData(data));
  return instance.get('', {
    params: data
  });
}

function addArchivos(data) {
  if (!data.dataToSend.file) {
    return Promise.resolve();
  }

  var dataToSend = {
    companyname: con.getUser().company,
    file: data.dataToSend.file,
    filesize: data.dataToSend.filesize,
    idobjeto: data.dataToSend.idobjeto,
    tags: data.dataToSend.tags,
    tipoobjeto: data.dataToSend.tipoobjeto
  };
  var formData = new FormData();

  for (var i in dataToSend) {
    formData.append(i.toUpperCase(), dataToSend[i]);
  }

  return axios.post(data.endpoint, formData, {
    baseURL: con.getUser().url,
    headers: {
      'Content-Type': 'multipart/form-data',
      "Authorization": "Basic ".concat(encode(con.getUser(), "userEncode"))
    }
  }).then(function (response) {
    var data = get(response, 'data', {});
    return data;
  });
}

function downLoad(data) {
  var formData = new FormData();
  var dataToSend = {
    companyname: con.getUser().company,
    id: data.dataToSend.id
  };

  for (var i in dataToSend) {
    formData.append(i.toUpperCase(), dataToSend[i]);
  }

  return axios.post(data.endpoint, formData, {
    baseURL: con.getUser().url,
    responseType: 'blob',
    headers: {
      'Content-Type': 'multipart/form-data',
      "Authorization": "Basic ".concat(encode(con.getUser(), "userEncode"))
    }
  }).then(function (response) {
    var dat = get(response, 'data', {});
    saveAs(new Blob([dat]), data.name);
    return dat;
  });
}

function destroyArchivo(data) {
  var url = "archivos/del";
  var instance = axiosUser(url, assignData(data));
  return instance["delete"]('/', {
    params: data
  });
}