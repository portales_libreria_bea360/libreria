const { axiosBea360 } = require('../../util/axios')
const axios = require('axios')
const { saveAs } = require('file-saver')
const get = require('lodash/get')
const { Instances } = require('../../util/instance')
const { encode } = require('../../util/methods')
const con = new Instances()

function getArchivos(data){

    let url = `casos/archivo/getby`;
    let instance = axiosBea360(url);
    return instance.get('/', { params: { idobjeto: data.idobjeto, tipoobjeto: data.tipoobjeto }});
}

function upload(data){
 
    if (!data.dataToSend.file) {
        return Promise.resolve()
    }
   
    let dataToSend = {
        companyname: con.getContacto().company,
        file: data.dataToSend.file,
        filesize: data.dataToSend.filesize,
        idobjeto: data.dataToSend.idobjeto,
        tags: data.dataToSend.tags,
        tipoobjeto: data.dataToSend.tipoobjeto
    }

    let formData = new FormData()
    for (let i in dataToSend){
        formData.append(i.toUpperCase(), dataToSend[i])
    }
    return axios.post(data.endpoint, formData, { baseURL: con.getContacto().url, headers: { 'Content-Type': 'multipart/form-data',  "Authorization":  `Basic ${encode(con.getContacto(), "token")}` } })
        .then((response)=>{
            let data = get(response, 'data', {})
            return data
        })
}

function downLoad(data){
    let formData = new FormData()
    let dataToSend = {
        companyname: con.getContacto().company,
        id: data.dataToSend.id
    }

    for (let i in dataToSend){
        formData.append(i.toUpperCase(), dataToSend[i])
    }
    
    return axios.post(data.endpoint, formData, { baseURL: con.getContacto().url, responseType: 'blob', headers: { 'Content-Type': 'multipart/form-data',  "Authorization":  `Basic ${encode(con.getContacto(), "token")}` } })
        .then((response)=>{
            let dat = get(response, 'data', {})
            saveAs(new Blob([dat]), data.name)
            return dat
        })

}

function destroyArchivo(data){
    let url = `casos/archivo/del`;
    let instance = axiosBea360(url);
    return instance.delete('/', { params: { id: data.id } });
}

export {
    getArchivos,
    upload,
    downLoad,
    destroyArchivo
}