/** funcion que trae todos los casos asociados a un contacto
 * por parametro llega el objeto casos que dentro contiene dos variables 
 * casos.url(base de datos del cliente) y casos.key(variable encriptada)
 * dentro la funcion invoca un metodo que hace la peticion GET al servidor bea360
 */
const { axiosBea360 } = require('../../util/axios')
const { cleanData } = require('../../util/methods')

function getAll(data) {
    let url = `casos/get`;
    let instance = axiosBea360(url)
    return instance.get()
}

function getBy(data){
    let url = `casos/getby`;
    let instance = axiosBea360(url);
    return instance.get('/', { params: { id: data.id } });
}

function addCase(data){
    let url = `casos/add`;
    let instance = axiosBea360(url)
    let params = cleanData(data)
    return instance.post('/', params)
}

function getProductos(data){
    let url= `casos/producto/get`;
    let instance = axiosBea360(url);
    return instance.get();

}

function getMotivos(data){
    let url = `casos/tipo/getby`;
    let instance = axiosBea360(url);
    return  instance.get('/', { params: { idproducto: data.idproducto }});
}

function getSubMotivos(data){
    let url = `casos/subtipo/getby`;
    let instance = axiosBea360(url);
    return instance.get('/', { params: { 
            idproducto: data.idproducto,
            idtipo: data.idtipo 
        }
    })
}

export {
    getAll,
    getBy,
    addCase,
    getProductos,
    getMotivos,
    getSubMotivos
}
