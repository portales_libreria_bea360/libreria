const { axiosBea360 } = require('../../util/axios')

function getAllCuenta(data){
    let url = `cuenta/get`
    let instance = axiosBea360(url)
    return instance.get()
}

export {
    getAllCuenta
}