/**
 * metodos mara mostrar indicadores de los casos totales, por mes, finlizados etc.
 */
function getIndicador(value) {
    let casos = value
    let casosTotal = {"total": casos.length}
    let finalizado = {"total": 0, "porcentaje": 0}
    let proceso = {"total": 0, "porcentaje": 0}
    let casosMes = {"total": 0}
    let indicadores;

    let hoy = new Date();
    let mes = hoy.getMonth()+1;
    let año = hoy.getFullYear();
    
    casos.forEach(caso => {
        if(caso.finalizado === 1){
            finalizado.total++
        }else{
            proceso.total++
        }
        if(parseInt(caso.fechacreacion.split('-')[1]) === mes && parseInt(caso.fechacreacion.split('-')[0]) === año){
            casosMes.total++
        }
    });

    /**porcentaje */
    
    finalizado.porcentaje = calculoPorcentaje(casosTotal.total, finalizado.total)
    proceso.porcentaje = calculoPorcentaje(casosTotal.total, proceso.total)

    return indicadores={
        casosTotal,
        finalizado,
        proceso,
        casosMes
    }
}

function calculoPorcentaje (total, finalizado){
    return Math.floor(((finalizado/total)*100)*100)/100
}

export {
    getIndicador
}
