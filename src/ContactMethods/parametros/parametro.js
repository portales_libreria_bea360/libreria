const { axiosBea360 } = require('../../util/axios')

function getParametros(data){
    let url = `prparametros/get`
    let instance = axiosBea360(url);
    return instance.get('', { params: { parametro: data.parametro }});
    
}

export {
    getParametros
}