const { axiosBea360 } = require('../../util/axios')

function getNotes(data){
    let url = `casos/notas/getby`;
    let instance = axiosBea360(url);
    return instance.get('/', { params: { idobjeto: data.idobjeto, tipoobjeto: data.tipoobjeto }});
}

function addNote(data){
    let url = `casos/notas/add`;
    let instance = axiosBea360(url)
    return instance.post('', {
        idobjeto: data.idobjeto,
        tipoobjeto: data.tipoobjeto,
        texto: data.texto
    })
}

export {
    getNotes,
    addNote
}