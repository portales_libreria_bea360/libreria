const { axiosIntegration } = require('../../util/axios')
const { paises } = require('./config')

function serviceRest(data){
    let dataBea360 = apiBea360(data) // buscar parametros endpoint a bea360
    let instance = axiosIntegration(dataBea360) // solicitud de servicio cliente
    switch (dataBea360.action.method){
        case 'get':
            if (typeof data.data === 'undefined'){
                return instance[dataBea360.action.method]('')
            }else {
                return instance[dataBea360.action.method]('', { params: data.data})
            }
    }
}

//metodo que busca los endpoint y action de cada servicio a bea360
// por el momento se trabaja con datos enduros.
function apiBea360(data){

    let action  = paises.actions.find((item)=> item.action === data.action)
    //return de todos datos del servicio
    return {
        baseURL: paises.baseURL,
        action: action
    }
}

export {
    serviceRest
}