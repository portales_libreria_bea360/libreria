/**funcion que permite hacer el login del usuario del portal
 * por parametro llega un objetoUser con tres variables 
 */
const axios = require('axios')
const { axiosBea360, axiosBea360Register } = require('../../util/axios')
const { cleanData, encode, cleanDataRecovery } = require('../../util/methods')
const { Instances } = require('../../util/instance')

function loginCp(data){  
    let user = {
        "company": data.company,
        "email": data.email,
        "pass": data.pass
    }
    try {
        return axios.post(`${data.url}/apir/v10/cp/auth/login`, user)
        .then((response)=>{

            if(response.data.contacto != null){
                const instance = new Instances()
                instance.createContacto({
                    contacto: response.data.contacto,
                    company: data.company,
                    url: data.url
                })
                return {
                    data: response.data
                }
            }     
        })
    } catch (e) {
        console.log(e)
        return null
    }
}

function logoutCp(data){
    const instance = new Instances()
    instance.deleteContacto(data)
}
function getByContact(data){
    let url = `perfil/getby`
    let instance = axiosBea360(url)
    let params = cleanData(data)
    return instance.get('', { params })
}

function addContact(data){
    let url = `auth/registro`
    let instance = axiosBea360Register(data, url)
    let params = cleanDataRecovery(data)
    return instance.post('', params)
}

function updContact(data){
    let url = `perfil/upd`;
    let instance = axiosBea360(url);
    let params = cleanData(data)
    return  instance.put('', params);
}

function updatePass(data){
    let url = `perfil/cambiopass`
    let instance = axiosBea360(url)
    let params = cleanData(data)
    return instance.put('', params)
}

function recoveryPass(data){
    //cambiar el metodo axios
    let url = `auth/crecovery`
    let instance = axiosBea360Register(data, url)
    let params = cleanDataRecovery(data)
    return instance.post('', params)
}

export {
    loginCp,
    logoutCp,
    getByContact,
    addContact,
    updContact,
    updatePass,
    recoveryPass
}
