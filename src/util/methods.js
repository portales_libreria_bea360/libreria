const _get = require('lodash/get')
const { Agente_cf,
        Agente_sla,
        Agente_prioridad,
        Agente_estado,
        Agente_check,
        Agente_proceso,
        Agente_cuenta,
        Agente_contacto,
        Agente_grupo,
        Agente_user,
        Agente_tipificacion } = require('../lib/agenteBea360')

function cleanData(data){
    let params = {}
    //codigo que extrae la key y url del objeto principal
    for (let i in data){
        if (i != 'url' && i != 'company' && i != 'token'){
            params[i] = data[i]
        }
    }
    return params
}

function cleanDataRecovery(data){
    let params = {}
    //codigo que extrae la key y url del objeto principal
    for (let i in data){
        if (i != 'url' && i != 'token'){
            params[i] = data[i]
        }
    }
    return params
}

/**
 * metodo que se encarga de asignar un valor a la data que se envia desde el front
 */

function assignData(data){
    return typeof data === 'undefined' ? {} : data
}

/**metodos de encriptacion de credenciales de usuario, en este se encripta en base64
 * la company, usuario y contraseña para que despues utilizarlo como cabecera en las llamadas
 * a los otros metodos de la API (casos, contactos y cuentas)
 */
function encode(key, condicion){
    switch (condicion){
        case 'auth':
            return new Buffer.from(`${key.company}/${key.email}:${key.pass || 'anonymous'}`).toString('base64');
        case 'token':
            return new Buffer.from(`${key.company}/${key.contacto.email[0]}:${key.contacto.pass || 'anonymous'}`).toString('base64');
        case 'anonymous':
            return new Buffer.from(`${key.company}/${condicion}:${condicion}`).toString('base64');
        case 'userEncode':
            //encode de agente
            return new Buffer.from(`${key.company}/${key.usuario.user}:${key.token}`).toString('base64')
        case 'userEncodeServe':
            //entra por servidor
            return new Buffer.from(`${key.company}/${key.usuario}:${key.token}`).toString('base64')
        default:
            return new Buffer.from(JSON.stringify(key)).toString('base64')
    }
}

/**Metodo para limpiar y extraer la data de las respuestas */

function cleanResponse(response){
    const data = _get(response, 'data.data', {})
    return data
}

/**metodo para desencriptar variables en base64 */

function decode(key){
    let buff = new Buffer.from(key, 'base64');
    let decodeBase64Key = buff.toString('ascii');
    return decodeBase64Key;
}

async function getAPIs(data, objeto, type){

    const { method, id } = data
    let Object = {}
    let ArrayService = []
    const arr = ['acuerdos', 'cfs', 'prioridades', 'estados', 'checklist', 'contactos', 'cuentas', 'tipificacion', 'procesos', 'pasos', 'usuarios','grupos']
    const tip = ['productos', 'motivos', 'submotivos']

    let services = method.filter(a => arr.find(b => b === a)) // filtro solo los que corresponden

    //reemplazo tipificación por el otro array.
    if (services.includes('tipificacion')) {
        let indice = services.indexOf('tipificacion')
        services.splice(indice, 1, ...tip)
    }

   let functionSer = services.map(ser => {
       return {
           name: ser,
           ...nameServicio(ser, objeto, type)
       }
   })

   ArrayService = await Promise.allSettled(
        functionSer.map(a =>{
            return a.objeto[a.function](a.data)
        }) 
    )
    let cont = 0
    for (let i in ArrayService) {
        if (services[cont] === 'productos' || services[cont] === 'motivos' || services[cont] === 'submotivos') {
            Object[services[cont]] = [ArrayService[i].value]
        } else if(services[cont] === 'contactos' || services[cont] === 'cuentas' || services[cont] === 'usuarios') {
            Object[services[cont]] = await addObject(ArrayService[i].value, objeto, services[cont])
        } else {
            Object[services[cont]] = ArrayService[i].value
        }
        cont++
    }
    return Object

}

async function addObject(allData, objeto, type) {
    if (type === 'contactos') {
        const contacto = new Agente_contacto()
        let match = allData.find(obj => obj.id === objeto.idcontacto) // busco que si el contacto del caso esta en los primeros 10 resultados de la API de contactosAll
        if (!match && objeto.idcontacto !== null) {
            const oneContacto = await contacto.one({ id: objeto.idcontacto })
            return allData.concat(oneContacto)  // uno los resultados
        } else {
            return allData
        }
    } else if (type === 'cuentas') {
        const cuenta = new Agente_cuenta()
        let match = allData.find(obj => obj.id === objeto.idcuenta) // busco que si el contacto del caso esta en los primeros 10 resultados de la API de contactosAll
        if (!match && objeto.idcuenta !== null) {
            const oneCuenta = await cuenta.one({ id: objeto.idcuenta })
            return allData.concat(oneCuenta)
        } else {
            return allData
        }

    } else if (type === 'usuarios'){
        const usuario = new Agente_user() 
        let match = allData.find(user => user.id === objeto.idusuarioasignado)
        if (!match && objeto.idusuarioasignado !== null) {
            const oneUser = await usuario.one({ id: objeto.idusuarioasignado })
            return allData.concat(oneUser)  // uno los resultados
        } else {
            return allData
        }

    }
}

function nameServicio(value, objeto, type) {

    const acuerdo = new Agente_sla()
    const cf = new Agente_cf()
    const prioridad = new Agente_prioridad() 
    const estado = new Agente_estado()
    const check = new Agente_check()
    const contacto = new Agente_contacto()
    const tip = new Agente_tipificacion()
    const proceso = new Agente_proceso()
    const cuenta = new Agente_cuenta()
    const grupo = new Agente_grupo()
    const usuario = new Agente_user()

    switch(value) {
        case 'acuerdos': 
            return {
                objeto: acuerdo,
                function: 'all'
            }
        case 'cfs': 
            return {
                objeto: cf,
                function: 'all',
                data: {
                    idtipoobjeto: typeObjectCf(type)
                }
            }
        case 'prioridades': 
            return {
                objeto: prioridad,
                function: 'all'
            }
        case 'estados': 
            return {
                objeto: estado,
                function: 'all'
            }
        case 'checklist': 
            return {
                objeto: check,
                function: 'all',
                data: {
                    idcaso: objeto.id // id del caso
                }
            }
        case 'contactos': 
            return {
                objeto: contacto,
                function: 'all',
                data: {
                    pagina: 1,
                    cantidad: 10
                }
            }
        case 'productos': 
            return {
                objeto: tip,
                function: 'productosOne',
                data: {
                    id: objeto.idproducto
                }
            }
        case 'motivos': 
            return {
                objeto: tip,
                function: 'motivosOne',
                data: {
                    id: objeto.idtipo
                }
            }
        case 'submotivos': 
            return {
                objeto: tip,
                function: 'subMotivosOne',
                data: {
                    id: objeto.idsubtipo
                }
            }
        case 'procesos': 
            return {
                objeto: proceso,
                function: 'all',
                data: {
                    activo: 1, cantidad: 10, pagina: 1 
                }
            }
        case 'cuentas': 
            return {
                objeto: cuenta,
                function: 'all',
                data: {
                    idcontacto: objeto.idcontacto, cantidad: 10, pagina: 1 
                }
            }
        case 'pasos': 
            return {
                objeto: proceso,
                function: 'stages',
                data: {
                    id: objeto.id
                }
            }
        case 'usuarios': 
            return {
                objeto: usuario,
                function: 'getdisponibles',
                data: {
                     pagina: 1, cantidad: 10 
                }
            }
        case 'grupos': 
            return {
                objeto: grupo,
                function: 'all',
                data: {
                    tipo: 2
                }
            }
    }
}

function typeObjectCf(type){
    switch(type){
        case 'caso': 
            return 1
        case 'contacto':
            return 2
        case 'cuenta':  
            return 3
    }
}

export {
    cleanData,
    cleanDataRecovery,
    assignData,
    encode,
    decode,
    cleanResponse,
    getAPIs
}



    // for(let i in method){
    //     if (method[i] === 'acuerdos') {
    //         const acuerdo = new Agente_sla()
    //         Object[method[i]] = await acuerdo.all()

    //     } else if (method[i] === 'cfs') {
    //         const cf = new Agente_cf()
    //         Object[method[i]] = await cf.all({ idtipoobjeto: typeObjectCf() })
            
    //     }   else if (method[i] === 'prioridades') {
    //         const prioridad = new Agente_prioridad() 
    //         Object[method[i]] = await prioridad.all()

    //     } else if (method[i] === 'estados'){
    //         const estado = new Agente_estado()
    //         Object[method[i]] = await estado.all()

    //     } else if (method[i] === 'checklist') {
    //         const check = new Agente_check()
    //         Object[method[i]] = await check.all({
    //             idcaso: id
    //         })
    //     } 
    //     else if (method[i] === 'grupos') {
    //         const grupo = new Agente_grupo()
    //          Object[method[i]] = await grupo.all({
    //             tipo: 2 // 1 = usuarios, 2 = contactos
    //         })
    //     }
    //     // desde aqui no se va a mostrar todavia
    //     else if (method[i] === 'contactos') {
    //         const contacto = new Agente_contacto()
    //         const allContactos = await contacto.all({ idcuenta: 0 }) // traigo todos los contactos

    //         let match = allContactos.find(contact => contact.id === objeto.idcontacto) // busco que si el contacto del caso esta en los primeros 10 resultados de la API de contactosAll
    //         if (!match && objeto.idcontacto !== null) {
    //             const oneContacto = await contacto.one({ id: objeto.idcontacto })
    //             Object[method[i]] = allContactos.concat(oneContacto)  // uno los resultados
    //         } else {
    //             Object[method[i]] = allContactos
    //         }
    //     } else if (method[i] === 'tipificacion') {
    //         const tip = new Agente_tipificacion()
    //         const productos = [await tip.productosOne({ id: objeto.idproducto })]
    //         const motivos = [await tip.motivosOne({id: objeto.idtipo})]
    //         const subMotivos = [await tip.subMotivosOne({id: objeto.idsubtipo })]

    //         Object['productos'] = productos
    //         Object['motivos'] = motivos
    //         Object['submotivos'] = subMotivos
 
    //     } else if (method[i] === 'cuentas') {
    //         const cuenta = new Agente_cuenta()
    //         const allCuentas = await cuenta.all({ pagina: 1, cantidad: 10 }) 

    //         let match = allCuentas.find(cuent => cuent.id === objeto.idcuenta)

    //         if (!match && objeto.idcuenta !== null) {
    //             const oneCuenta = await cuenta.one({ id: objeto.idcuenta })
    //             Object[method[i]] = allCuentas.concat(oneCuenta)  // uno los resultados
    //         } else {
    //             Object[method[i]] = allCuentas
    //         }
    //     } else if (method[i] === 'procesos') {
    //         const proceso = new Agente_proceso()
    //         const allProcesos = await proceso.all({ activo: 1, cantidad: 10, pagina: 1 })
    //         Object[method[i]] = allProcesos
    //     } else if (method[i] === 'pasos') {
    //         const proceso = new Agente_proceso()
    //         const allStages = await proceso.stages({
    //              id: objeto.id
    //         })
    //         Object[method[i]] = allStages
    //     } else if (method[i] ==='usuarios') {
    //         // hacer logica para cuenado el usuario no esta entre los prineros 10
    //         const usuario = new Agente_user()
    //         const allUsuarios = await usuario.getdisponibles({ pagina: 1, cantidad: 10 })

    //         let match = allUsuarios.find(user => user.id === objeto.idusuarioasignado)

    //         if (!match && objeto.idusuarioasignado !== null) {
    //             const oneUser = await usuario.one({ id: objeto.idusuarioasignado })
    //             Object[method[i]] = allUsuarios.concat(oneUser)  // uno los resultados
    //         } else {
    //             Object[method[i]] = allUsuarios
    //         }
    //     }
    // }
    // return Object
