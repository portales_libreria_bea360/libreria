/**Metodo base axios que recibe un objeto con dos variables
 * casos.url(base de datos del cliente) y casos.key(variable encriptada)
 * tambien recibe otro parametro que es para completar el resto de url
 * cuando un contacto se esta registrando se genera un token con con el company/anonymous:anonymous
*/
const axios = require('axios');
const { encode } = require('./methods')
const { Instances } = require('./instance')

function axiosBea360(url, data){
    const con = new Instances()
    if (typeof con.getContacto() != 'undefined' && Object.keys(con.getContacto()).length !== 0){
        return axios.create({
            baseURL: `${con.getContacto().url}/apir/v10/cp/${url}`,
            headers :{
                "Content-Type": "application/json",
                "Authorization":  `Basic ${encode(con.getContacto(), "token")}`
            }
        })
    }
}

function axiosBea360Register(data, url){
    return axios.create({
        baseURL: `${data.url}/apir/v10/cp/${url}`,
        headers :{
            "Content-Type": "application/json",
            "Authorization":  `Basic ${encode(data, "anonymous")}`
        }
    })
}

//metodos custom con el portal bea360
function axiosCustom(data){
    let instance= axiosBea360(data.endpoint)
    return instance[data.method]('', data.dataToSend)
}

function axiosIntegration({baseURL, action}){
    let instance = axios.create({
        baseURL: `${baseURL}/${action.path}`,
        headers :{
            "Content-Type": "application/json",
            "x-rapidapi-host":  `${action.key1}`,
            "x-rapidapi-key" : `${action.key2}`
        }
    })
   return instance
}

//Axios agente bea360

function axiosUser(url, data){
    const con = new Instances()
    //si el token no existe
    if(typeof data.token === 'undefined'){
        //entra por navegador
        let instance = axios.create({
            baseURL: `${con.getUser().url}/apir/v11/${url}`,
            headers: {
                "Content-Type": "application/json",
                "Authorization":  `Basic ${encode(con.getUser(), "userEncode")}`
            }
        })
        return instance
    } else {
        /**
         * entra por servidor
         * en la data viene un token
         */
        let instance = axios.create({
            baseURL: `${data.url}/apir/v11/${url}`,
            headers: {
                "Content-Type": "application/json",
                "Authorization":  `Basic ${encode(data, "userEncodeServe")}`
            }
        })
        return instance
    }

}

export {
    axiosBea360,
    axiosBea360Register,
    axiosCustom,
    axiosIntegration,
    axiosUser
}