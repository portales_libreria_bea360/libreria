const jwt = require('jsonwebtoken');
const CryptoJS = require('crypto-js/crypto-js');
/**
 * metodos para crear instancia de Usuario
 * token: res.data.data.token,
    permisos: res.data.data.permisos,
    usuario: res.data.data.usuario,
    url: url,
    company:company
 */

class Instances {
    createUser(data){
        let Object = {}
        let { token, permisos, usuario, url, company } = data
        // normal encript
        if (data) {
            localStorage.setItem('USERINSTANCE', JSON.stringify(encryptAES(data,"abcdefgabcdefg12")))
        }

        // full encript 
        // for (let i in data){
        //     Object[encryptAES(i, "abcdefgabcdefg12")] = encryptAES(data[i],"abcdefgabcdefg12")
        // }

        // if (data) {
        //     localStorage.setItem('USERINSTANCE', JSON.stringify(encryptAES(Object, "abcdefgabcdefg13")))
        // }
    }
    getUser(value){
        let Object = {}

        // normal decript
        let data = JSON.parse(decrypAES(JSON.parse(localStorage.getItem('USERINSTANCE')), "abcdefgabcdefg12"))
        let { token, permisos, usuario, url, company } = data

        if (value) {
            return {
                usuario: data.usuario,
                permisos: data.permisos,
                company: data.company
            }
        } else {
            return data
        }
        
        // let data = JSON.parse(decrypAES(JSON.parse(localStorage.getItem('USERINSTANCE')), "abcdefgabcdefg13"))
        // let { token, permisos, usuario, url, company } = data
        // for (let i in data){
        //     Object[JSON.parse(decrypAES(i, "abcdefgabcdefg12"))] = JSON.parse(decrypAES(data[i], "abcdefgabcdefg12"))
        // }

        // if (value) {
        //     return {
        //         usuario: Object.usuario,
        //         permisos: Object.permisos,
        //         company: Object.company
        //     }
        // } else {
        //     return Object
        // }
    }

    //metodos de portal contacto
    /** variables encriptadas
     *  contacto: response.data.contacto,
        company: data.company,
        url: data.url
     */
    createContacto(data){
        if (data) {
            let token = jwt.sign(data, 'clave')
            localStorage.setItem('INSTANCE', JSON.stringify(token))
        }
    }

    getContacto(){
        return jwt.verify(JSON.parse(localStorage.getItem('INSTANCE')), 'clave', (err, decoded)=>{
            return decoded
        })
    }

    deleteContacto(data){
        if (data){
            let data = jwt.verify(JSON.parse(localStorage.getItem('INSTANCE')), 'clave', (err, decoded)=>{
                let objeto = {}
                for (let i in decoded){
                    if (i === 'contacto'){
                        objeto[i] = decoded[i]
                    }
                }
                return objeto
            })
            let token = jwt.sign(data, 'clave')
            localStorage.setItem('INSTANCE', JSON.stringify(token))
            
        } else {
            //deslogueo sin guardar nada
            localStorage.removeItem('INSTANCE')
        }
    }

    getInstance(){
        let data = JSON.parse(localStorage.getItem('INSTANCESERVER'))
        return data
    }
}

function encryptAES(data, llave) {
    let key = CryptoJS.enc.Utf8.parse(llave);
    let srcs = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
    return CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7}).toString();
}

function decrypAES(data, llave) {
    let key = CryptoJS.enc.Utf8.parse(llave);
    let decrypt = CryptoJS.AES.decrypt(data, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
    return CryptoJS.enc.Utf8.stringify(decrypt).toString();
}

export {
    Instances
}