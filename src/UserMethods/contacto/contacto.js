const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllContacto(data){
    let url = `contacto/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getByContacto(data){
    let url = `contacto/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function addContacto(data){
    let url = `contacto/add`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
}

function updContacto(data){
    let url = `contacto/upd`;
    let instance = axiosUser(url, assignData(data))
    return instance.put('', data)
}

export {
    getAllContacto,
    getByContacto,
    addContacto,
    updContacto
}