const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllUsuario(data){
    let url = `usuario/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getByUsuario(data){
    let url = `usuario/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getUsuarioDisponible(data){
    let url = `usuario/getdisponibles`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

export {
    getAllUsuario,
    getByUsuario,
    getUsuarioDisponible
}