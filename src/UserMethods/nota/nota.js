const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllNotas(data){
    let url = `actividad/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function addNotas(data){
    let url = `actividad/add`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
}

export {
    getAllNotas,
    addNotas
}