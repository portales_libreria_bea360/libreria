const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllChecklist(data){
    let url = `checklist/tareas/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}


function executescriptCheck(data){
    let url = `checklist/tareas/executescript`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}
// estyos metodos se accionan con los check pero pertenecen casos
function doneCheck(data){
    let url = `caso/done`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
}

function undoneCheck(data){
    let url = `caso/undone`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
}
export {
    getAllChecklist,
    executescriptCheck,
    doneCheck,
    undoneCheck
}