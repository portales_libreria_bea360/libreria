const axios = require('axios')
const { Instances } = require('../../util/instance')

function userToken(data){
    let { id, company, url } = data

    let instance = axios.create({
        baseURL: `${url}/apir/v11/login/auth/tokenvalidate`,
        headers :{
            "Content-Type": "application/json",
        }
    })
 
    return instance.post('',{
        id,
        company
    })
    .then((res)=>{
        if (res.data.data){
            const instance = new Instances()
            instance.createUser({
                token: res.data.data.token,
                permisos: res.data.data.permisos,
                usuario: res.data.data.usuario,
                url: url,
                company:company
            })
            return {
                ok: true
            }
        }
    })
}
//metodo 
function userLogin (data){

    let { user, pass, company, url, local, type } = data

    let instance = axios.create({
        baseURL: `${url}/apir/v11/login/auth`,
        headers :{
            "Content-Type": "application/json",
        }
    })

    return instance.post('',{
        user,
        pass,
        company
    })
    .then((res)=>{
        
        if (res.data){    
            //para ocupar la libreria en un backend NODE
            if (type === 'back'){
                return {
                    token: res.data.token,
                    permisos: res.data.permisos,
                    usuario: res.data.usuario,
                    url: url,
                    company: company,
                    type: type
                }
            } else {
                // si es para front, la libreria se encarga de crear y guardar los datos en localStorage
                const con = new Instances()
                con.createUser({
                    token: res.data.token,
                    permisos: res.data.permisos,
                    usuario: res.data.usuario,
                    url: url,
                    company:company
                })
                return res.data
            }
            
        }
    })
}

function authwithtoken (data){

    let { url, token, company } = data

    let instance = axios.create({
        baseURL: `${url}/apir/v11/login/authwithtoken`,
        headers :{
            "Content-Type": "application/json",
        }
    })

    return instance.post('',{token})
    .then((res)=>{
        if (res.data){    
            // si es para front, la libreria se encarga de crear y guardar los datos en localStorage
            const con = new Instances()
            con.createUser({
                token: res.data.token,
                permisos: res.data.permisos,
                usuario: res.data.usuario,
                url: url,
                company:company
            })
            return res.data   
        }
    })
}

export {
    userToken,
    userLogin,
    authwithtoken
}