const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getByGruposSkill(data){
    let url = `gruposkill/listar/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

export {
    getByGruposSkill
}