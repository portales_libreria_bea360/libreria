const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllCuentas(data){
    let url = `cuenta/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getByCuenta(data){
    let url = `cuenta/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function addCuenta(data){
    let url = `cuenta/add`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
}

function updCuenta(data){
    let url = `cuenta/upd`;
    let instance = axiosUser(url, assignData(data))
    return instance.put('', data)
}

function getContacts(data){
    let url = `cuenta/getcontactsbyid`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

export {
    getAllCuentas,
    getByCuenta,
    addCuenta,
    updCuenta,
    getContacts
}