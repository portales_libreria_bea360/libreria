const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getParametros(data){
    let url = `prparametros/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

export {
    getParametros
}