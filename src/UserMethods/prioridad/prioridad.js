const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllPrioridad(data){
    let url = `prioridad/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get()
}

export {
    getAllPrioridad
}