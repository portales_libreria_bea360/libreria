const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllEstados(data){
    let url = `estado/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get()
}

export {
    getAllEstados
}