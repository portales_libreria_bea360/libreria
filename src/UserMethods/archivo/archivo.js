const { axiosUser } = require('../../util/axios')
const axios = require('axios')
const { saveAs } = require('file-saver')
const get = require('lodash/get')
const { assignData } = require('../../util/methods')
const { Instances } = require('../../util/instance')
const { encode } = require('../../util/methods')
const con = new Instances()

function getAllArchivos(data){
    let url = `archivos/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function addArchivos(data){
    if (!data.dataToSend.file) {
        return Promise.resolve()
    }

    let dataToSend = {
        companyname: con.getUser().company,
        file: data.dataToSend.file,
        filesize: data.dataToSend.filesize,
        idobjeto: data.dataToSend.idobjeto,
        tags: data.dataToSend.tags,
        tipoobjeto: data.dataToSend.tipoobjeto
    }

    let formData = new FormData()
    for (let i in dataToSend){
        formData.append(i.toUpperCase(), dataToSend[i])
    }
    return axios.post(data.endpoint, formData, { 
        baseURL: con.getUser().url,
        headers: { 
            'Content-Type': 'multipart/form-data',
             "Authorization":  `Basic ${encode(con.getUser(), "userEncode")}` 
            } 
        }).then((response)=>{
            let data = get(response, 'data', {})
            return data
        })
}

function downLoad(data){
    let formData = new FormData()
    let dataToSend = {
        companyname: con.getUser().company,
        id: data.dataToSend.id
    }

    for (let i in dataToSend){
        formData.append(i.toUpperCase(), dataToSend[i])
    }

    return axios.post(data.endpoint, formData, { 
        baseURL: con.getUser().url,
        responseType: 'blob',
        headers: {
            'Content-Type': 'multipart/form-data',
            "Authorization":  `Basic ${encode(con.getUser(), "userEncode")}` 
        } 
    }).then((response)=>{
        let dat = get(response, 'data', {})
        saveAs(new Blob([dat]), data.name)
        return dat
    })
}

function destroyArchivo(data){
    let url = `archivos/del`;
    let instance = axiosUser(url, assignData(data));
    return instance.delete('/', { params: data });
}

export {
    getAllArchivos,
    addArchivos,
    downLoad,
    destroyArchivo
}