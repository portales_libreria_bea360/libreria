const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllProductos(data){
    let url = `producto/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getOneProductos(data){
    let url = `producto/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getAllMotivos(data){
    let url = `tipo/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getOneMotivos(data){
    let url = `tipo/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function getAllSubMotivos(data){
    let url = `subtipo/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })  
}

function getOneSubMotivos(data){
    let url = `subtipo/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

export {
    getAllProductos,
    getAllMotivos,
    getAllSubMotivos,
    getOneProductos,
    getOneMotivos,
    getOneSubMotivos
}
