const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllAcuerdo(data){
    let url = `sla/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get()
}

export {
    getAllAcuerdo
}