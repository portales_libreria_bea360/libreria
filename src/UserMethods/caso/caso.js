const { axiosUser } = require('../../util/axios')
const { cleanData, assignData } = require('../../util/methods')

function getAll(data) {
    let url = `caso/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data }) //variables limpias para hacer peticion
}

function getBy(data) {
    let url = `caso/getby`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function asignadoGrupo(data) {
    let url = `caso/asignadoagrupo/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

function updCase(data) {
    let url = `caso/upd`;
    let instance = axiosUser(url, assignData(data))
    return instance.put('', data)
}

function addCase(data){
    let url = `caso/add`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
}


export {
    getAll,
    getBy,
    addCase,
    updCase,
    asignadoGrupo
}