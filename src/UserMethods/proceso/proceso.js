const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllProcesos(data){
    let url = `workflow/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
} 

function nextCase(data){
    let url = `workflow/next`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
} 
function backCase(data){
    let url = `workflow/back`;
    let instance = axiosUser(url, assignData(data))
    return instance.post('', data)
} 
function stagesCase(data){
    let url = `workflow/stages`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
} 

export {
    getAllProcesos,
    nextCase,
    backCase,
    stagesCase
}