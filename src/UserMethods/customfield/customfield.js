const { axiosUser } = require('../../util/axios')
const { assignData } = require('../../util/methods')

function getAllCfs(data){
    
    let url = `cfcustomfield/get`;
    let instance = axiosUser(url, assignData(data))
    return instance.get('', { params: data })
}

export {
    getAllCfs
}