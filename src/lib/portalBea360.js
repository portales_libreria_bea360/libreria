const { getIndicador } = require('../ContactMethods/indicadores/indicador')
const { loginCp, logoutCp, getByContact, addContact, updContact, updatePass, recoveryPass } = require('../ContactMethods/auth/auth')
const { getAll, getBy, addCase, getProductos, getMotivos, getSubMotivos } = require('../ContactMethods/casos/caso')
const { getAllCuenta } = require('../ContactMethods/cuentas/cuenta')
const { getNotes, addNote } = require('../ContactMethods/notas/nota')
const { getParametros } = require('../ContactMethods/parametros/parametro')
const { getArchivos, upload, downLoad, destroyArchivo } = require('../ContactMethods/archivos/archivo')
const { axiosCustom } = require('../util/axios')
const { serviceRest } = require('../ContactMethods/integraciones/integracion')
const { Instances } = require('../util/instance')

class Auth{
    login(data){
        return loginCp(data)
    }
    logout(data){
        return logoutCp(data)
    }
    getBy(data){
        return getByContact(data)
    }
    one(value){
        const instance = new Instances()
        switch(value){
            case 'contacto':
                return instance.getContacto().contacto
            case 'company':
                return instance.getContacto().company
            default:
                return instance.getContacto()
        }
    }
    add(data) {
        return addContact(data)
    }
    upd(data){
        return updContact(data)
    }
    updPass(data){
        return updatePass(data)
    }
    recovery(data){
        return recoveryPass(data)
    }
}

class Custom {
    service (data){
        return axiosCustom(data)
    }
}

class Caso {
    all(data) {
        return  getAll(data)
    }
    one (data) {
        return  getBy(data)
    }
    add (data) {
        return addCase(data)
    }
    indicadores(data) {
        return getIndicador(data)
    }
    productos(data) {
        return getProductos(data)
    }
    motivos(data) {
        return getMotivos(data)
    }
    subMotivos(data) {
        return getSubMotivos(data)
    }
}

class Cuenta {
    all (data) {
        return getAllCuenta(data)
    }
}

class Nota {
    all(data){
        return getNotes(data)
    }
    add(data){
        return addNote(data)
    }
}

class Archivo {
    all(data){
        return  getArchivos(data)
    }
    add(data){
        return  upload(data)
    }
    download(data){
        return downLoad(data)
    }
    destroy(data){
        return destroyArchivo(data)
    }
}

class Parametro {
    all(data){
        return  getParametros(data)
    }
}

class Integracion {
    rest(data){
        return serviceRest(data)
    }
    iframe(data){
        return serviceIframe(data)
    } 
    addRest(data){
        return serviceAddRest(data)
    }
    addIframe(data){
        return serviceAddIframe(data)
    }
}

export {
    Auth,
    Custom,
    Caso,
    Cuenta,
    Nota,
    Archivo,
    Parametro,
    Integracion
}