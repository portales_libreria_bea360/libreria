const { userToken, userLogin, authwithtoken } = require('../UserMethods/auth/auth')
const { getAll, getBy, updCase, addCase, asignadoGrupo } = require('../UserMethods/caso/caso')
const { getAllNotas, addNotas } = require('../UserMethods/nota/nota')
const { getAllArchivos, addArchivos, downLoad, destroyArchivo } = require('../UserMethods/archivo/archivo')
const { getAllProductos, getAllMotivos, getAllSubMotivos, getOneProductos, getOneMotivos, getOneSubMotivos } = require('../UserMethods/tipificacion/tipificacion')
const { getAllPrioridad } = require('../UserMethods/prioridad/prioridad')
const { getAllEstados } = require('../UserMethods/estado/estado')
const { getAllContacto, getByContacto, addContacto, updContacto } = require('../UserMethods/contacto/contacto')
const { getAllUsuario, getByUsuario, getUsuarioDisponible } = require('../UserMethods/usuario/usuario')
const { getAllAcuerdo } = require('../UserMethods/acuerdoSla/acuerdoSla')
const { getByGruposSkill } = require('../UserMethods/grupoSkill/grupoSkill')
const { getAllCuentas, getByCuenta, addCuenta, updCuenta, getContacts } = require('../UserMethods/cuenta/cuenta')
const { getAllProcesos, nextCase, backCase, stagesCase } = require('../UserMethods/proceso/proceso')
const { getAllCfs } = require('../UserMethods/customfield/customfield')
const { encode, decode, cleanResponse, getAPIs } = require('../util/methods')
const { getAllChecklist, executescriptCheck, doneCheck, undoneCheck } = require('../UserMethods/checklist/index')
const { getParametros } = require('../UserMethods/parametro/index')
const { Instances } = require('../util/instance')
 
class Agente_auth {
    async loginToken(data){
        return cleanResponse(await userToken(data))
    }
    async login(data){
        return cleanResponse(await userLogin(data))
    }
    async loginSE(data){
        //return cleanResponse(await authwithtoken(data))
        return authwithtoken(data)
    }
    one(data){
        const instance = new Instances()
        switch(data){
            case 'usuario':
                return instance.getUser().usuario
            case 'company':
                return instance.getUser().company
            case 'permiso':
                return instance.getUser().permisos
            default:
                return instance.getUser('all')
        }
    }
}

class Agente_caso {
    async all(data){
        return cleanResponse(await getAll(data))
    }
    async one(data) {
        const { method, id } = data 
        const caso = cleanResponse(await getBy({ id: id })) // voy a buscar el caso
        const APIS = await getAPIs(data, caso, 'caso')
        if (method) {
            try {
                return{
                    data: caso, // siempre devuelve la data del caso
                    ...APIS // APIS son atrbutos variables dependiendo el arreglo
                }
            } catch (error) {
                console.log("error", error);
            }
        } else {
            return caso
        }
    }
    async add(data){
        return cleanResponse(await addCase(data))
    }
    async upd(data) {
        return cleanResponse(await updCase(data))
    }
    async asignadoagrupo(data) {
        return cleanResponse(await asignadoGrupo(data))
    }
}

class Agente_nota {
    async all(data){
        return cleanResponse(await getAllNotas(data))
    }
    async add(data){
        return cleanResponse(await addNotas(data))
    }
}

class Agente_archivo {
    async all(data){
        return cleanResponse(await getAllArchivos(data))
    }
    async add(data){
        return cleanResponse(await addArchivos(data))
    }
    async download(data){
        return cleanResponse(await downLoad(data))
    }
    async destroy(data){
        return cleanResponse(await destroyArchivo(data))
    }
}

class Agente_tipificacion {
    async productosAll(data) {
        return cleanResponse(await getAllProductos(data))
    }
    async productosOne(data) {
        return cleanResponse(await getOneProductos(data))
    }
    async motivosAll(data) {
        return cleanResponse(await getAllMotivos(data))
    }
    async motivosOne(data) {
        return cleanResponse(await getOneMotivos(data))
    }
    async subMotivosAll(data) {
        return cleanResponse(await getAllSubMotivos(data))
    }
    async subMotivosOne(data) {
        return cleanResponse(await getOneSubMotivos(data))
    }
}

class Agente_prioridad {
    async all(data){
        return cleanResponse(await getAllPrioridad(data))
    }
}

class Agente_estado {
    async all(data){
        return cleanResponse(await getAllEstados(data))
    }
}

class Agente_user {
    async all(data){
        return cleanResponse(await getAllUsuario(data))
    }
    async one(data){
        return cleanResponse(await getByUsuario(data))
    }
    async getdisponibles(data){
        return cleanResponse(await getUsuarioDisponible(data))
    }
}

class Agente_contacto {
    async all(data){
        return cleanResponse(await getAllContacto(data))
    } 
    async one(data){
        const { method, id } = data
        const contacto = cleanResponse(await getByContacto({ id: id })) // voy a buscar el contacto
        if (method) {
            const APIS = await getAPIs(data, contacto, 'contacto')
            try {
                return{
                    data: contacto, // siempre se devuelve la data del contacto
                    ...APIS // APIS que son variables dependiendo el arreglo
                }
            } catch (error) {
                console.log("error", error);
            }
        } else {
            return contacto
        }
    }
    async add(data){
        return cleanResponse(await addContacto(data))
    }
    async upd(data){
        return cleanResponse(await updContacto(data))
    }
}

class Agente_sla {
    async all(data){
        return cleanResponse(await getAllAcuerdo(data))
    }
}

class Agente_grupo {
    async all(data){
        return cleanResponse(await getByGruposSkill(data))
    }
}

class Agente_cuenta {
    async all(data){
        return cleanResponse(await getAllCuentas(data))
    }
    async one(data){
        const { method, id } = data
        const cuenta = cleanResponse(await getByCuenta({ id: id })) // voy a buscar la cuenta
        const APIS = await getAPIs(data, cuenta, 'cuenta')
        if (method) {
            try {
                return {
                    data: cuenta, // siempre se devuelve la cuenta
                    ...APIS // APIS que son variables dependiendo el arreglo
                }
            } catch (error) {
                console.log("error", error);
            }
        } else {
            return cuenta
        }
        //return getByCuenta(data)
    }
    async add(data){
        return cleanResponse(await addCuenta(data))
    }
    async upd(data){
        return cleanResponse(await updCuenta(data))
    }
    async getcontactsbyid(data) {
        return cleanResponse(await getContacts(data))
    }
}

class Agente_proceso {
    // aqui crear el next y back de procesos.
    async all(data){
        return cleanResponse(await getAllProcesos(data))
    }
    async next(data){
        return cleanResponse(await nextCase(data))
    }
    async back(data){
        return cleanResponse(await backCase(data))
    }
    async stages(data){
        return cleanResponse(await stagesCase(data))
    }
}

class Agente_check {
    async all(data){
        return cleanResponse(await getAllChecklist(data))
    }
    async check(data){
        let { idcaso, idtarea, origin, value } = data
        if (value === true || value === 1) {
            return cleanResponse(await doneCheck({idcaso, idtarea, origin}))
        } else {
            return cleanResponse(await undoneCheck({idcaso, idtarea, origin}))
        }
    }
    async executescript(data){
        return cleanResponse(await executescriptCheck(data))
    }
}

class Agente_cf {
    async all(data){
        return cleanResponse(await getAllCfs(data))
    }
}

class Agente_parametro {
    async all(data){
        return cleanResponse(await getParametros(data))
    }
}

class Util {
    encode(data){
        return encode(data)
    }

    decode(data){
        return decode(data)
    }
}

export {
    Agente_auth,
    Agente_caso,
    Agente_tipificacion,
    Agente_contacto,
    Agente_user,
    Agente_prioridad,
    Agente_estado,
    Agente_cuenta,
    Agente_proceso,
    Agente_cf,
    Agente_sla,
    Agente_grupo,
    Agente_nota,
    Agente_archivo,
    Agente_check,
    Agente_parametro,
    Util
}